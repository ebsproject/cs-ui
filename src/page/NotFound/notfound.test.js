import NotFound from "./NotFoundView";
import React from "react";
import { render, cleanup } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("NotFound is in the DOM", async () => {
  const { getByTestId } = render(<NotFound />);
  expect(getByTestId("notFoundTestId")).toBeInTheDocument();
});