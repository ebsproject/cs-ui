import React, { memo } from "react";
import { Core } from "@ebs/styleguide";
const { Button } = Core;

function NotFoundView(props) {
  return (
    <div
      className="bg-gradient-to-r from-ebs-green-900 to-ebs-green-default"
      data-testid={"notFoundTestId"}
    >
      <div className="w-9/12 m-auto py-16 min-h-screen flex items-center justify-center">
        <div className="w-lg bg-white shadow overflow-hidden sm:rounded-lg pb-8">
          <div className="border-t border-gray-200 text-center pt-8">
            <h1 className="text-128 font-bold text-ebs-green-default">404</h1>
            <h1 className="text-32 font-medium text-grey-800 pb-8">
              oops! Page not found.
            </h1>
            <Button
              variant="outlined"
              color="primary"
              href="/"
              className="w-128"
            >
              Go Home
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default memo(NotFoundView);
