import React, { memo } from "react";
import { FormattedMessage } from "react-intl";
import { EbsTabsLayout } from "@ebs/styleguide";
import Tenants from "components/organism/Tenants";
// import Organizations from "components/organism/Organizations";
// import Customers from "components/organism/Customers";
import Domains from "components/organism/Domains";
function TenantManagementView(props) {
  return (
    <EbsTabsLayout
      data-testid="tenantManagementTestid"
      tabs={[
        {
          label: <FormattedMessage id="none" defaultMessage="Tenants" />,
          component: <Tenants />,
        },
        {
          label: <FormattedMessage id="none" defaultMessage="Domains" />,
          component: <Domains />,
        },
        // {
        //   label: <FormattedMessage id="none" defaultMessage="Customers" />,
        //   component: <Customers />,
        // },
        // {
        //   label: <FormattedMessage id="none" defaultMessage="Organizations" />,
        //   component: <Organizations />,
        // },
      ]}
    />
  );
}
// Type and required properties
TenantManagementView.propTypes = {};
// Default properties
TenantManagementView.defaultProps = {};

export default memo(TenantManagementView);
