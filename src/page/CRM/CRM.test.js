import CRM from "./CRMView";
import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";

afterEach(cleanup);

test("CRM is in the DOM", async () => {
  const { getByTestId } = render(<CRM />, { wrapper: Wrapper });
  expect(getByTestId("CRMTestId")).toBeInTheDocument();
});
