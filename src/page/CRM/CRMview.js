import React, { memo } from "react";
import PropTypes from "prop-types";
import { EbsTabsLayout } from "@ebs/styleguide";
import People from "components/organism/People";
import Institutions from "components/organism/Institutions";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
/*
  @param { }: page props,
*/
function CRMView({}) {
  /*
  @prop data-testid: Id to use inside crm.test.js file.
 */
  return (
    <div data-testid={"CRMTestId"}>
      <EbsTabsLayout
        tabs={[
          {
            label: <FormattedMessage id="none" defaultMessage="People" />,
            component: <People />,
          },
          {
            label: <FormattedMessage id="none" defaultMessage="Institutions" />,
            component: <Institutions />,
          },
        ]}
      />
    </div>
  );
}
export default memo(CRMView);
// Type and required properties
CRMView.propTypes = {};
// Default properties
CRMView.defaultProps = {};
