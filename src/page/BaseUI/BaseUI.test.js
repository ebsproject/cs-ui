  import BaseUI from './BaseUI';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('BaseUI is in the DOM', () => {
  render(<BaseUI></BaseUI>)
  expect(screen.getByTestId('BaseUITestId')).toBeInTheDocument();
})
