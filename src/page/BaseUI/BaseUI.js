import React, { lazy, Suspense, memo } from "react";

const BaseUI = lazy(() =>
  System.import("@ebs/ui").then((module) => ({ default: module.App }))
);

function BaseUIView({}) {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <BaseUI />
    </Suspense>
  );
}
export default memo(BaseUIView);
