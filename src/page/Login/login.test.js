import Login from "./LoginView";
import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";

afterEach(cleanup);

test("Login is in the DOM", async () => {
  const { getByTestId } = render(<Login />, { wrapper: Wrapper });
  expect(getByTestId("loginTestId")).toBeInTheDocument();
});
