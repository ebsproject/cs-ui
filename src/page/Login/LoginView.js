import React, { memo } from "react";
import EBSAnimate from "components/atoms/EBSAnimate";
import { Core } from "@ebs/styleguide";
import UserMessage from "components/atoms/Message";
import Logo from "assets/images/logos/EBS_Vertical_2.png";
import Domains from "assets/images/branding/ebs_domains.png";
const { AppBar, Toolbar, List, ListItem, Button, Typography } = Core;

function LoginView({}) {
  function handleLogin() {
    let url = `${process.env.REACT_APP_AUTH_CONFIG_AUTH_URL}?client_id=${process.env.REACT_APP_AUTH_CONFIG_CLIENT_ID}&scope=openid&`;
    const redirectUrl = `redirect_uri=${process.env.REACT_APP_AUTH_CONFIG_CALLBACK_URL}&response_type=code`;
    url += redirectUrl;
    window.location.href = url;
  }

  return (
    <div className="relative" data-testid={"loginTestId"}>
      <div
        className="h-screen bg-ebs-green-default"
        style={{
          clipPath: "polygon(82% 0, 100% 0, 100% 100%, 70% 100%)",
        }}
      />
      <div className="absolute top-0 left-0 w-full">
        <UserMessage />
        <AppBar
          color="transparent"
          elevation={0}
          className={"static h-16 w-full"}
        >
          <Toolbar className="flex justify-between items-center">
            <EBSAnimate animation="transition.expandIn">
              <img className={"m-8"} width="180" src={Logo} alt="logo" />
            </EBSAnimate>
            <nav>
              <List className="flex">
                <ListItem className="bg-transparent">
                  <Button href="#" onClick={(e) => e.preventDefault()}>
                    <Typography className="text-ebs">Products</Typography>
                  </Button>
                </ListItem>
                <ListItem className="bg-transparent">
                  <Button href="#" onClick={(e) => e.preventDefault()}>
                    <Typography className="text-ebs" noWrap>
                      About us
                    </Typography>
                  </Button>
                </ListItem>
                <ListItem className="bg-transparent">
                  <Button href="#" onClick={(e) => e.preventDefault()}>
                    <Typography className="text-ebs">Contact</Typography>
                  </Button>
                </ListItem>
              </List>
            </nav>
            <Button
              className="w-1/6 bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
              aria-label="Login"
              onClick={handleLogin}
            >
              Login
            </Button>
          </Toolbar>
        </AppBar>
        <div className="sm:w-2/3 sm:pl-20 sm:pt-18">
          <EBSAnimate animation="transition.slideUpIn" delay={300}>
            <Typography
              color="inherit"
              paragraph={true}
              className="font-bold text-green-900 text-4xl sm:pt-10"
            >
              Enterprise Breeding System
            </Typography>
          </EBSAnimate>
          <EBSAnimate delay={200}>
            <Typography
              align="justify"
              paragraph={true}
              className="font-ebs sm:w-2/3 sm:pt-10"
            >
              The Enterprise Breeding System (EBS) is an open-source breeding
              informatics software being developed for crop breeding programs
              serving resource-poor farmers in Africa, Asia and Latin America.
            </Typography>
          </EBSAnimate>
          <EBSAnimate delay={400}>
            <Typography
              paragraph={true}
              align="justify"
              className="font-ebs sm:w-2/3"
            >
              The EBS connects, merges and builds upon existing breeding
              software and data solutions to offer a single powerful tool, so
              that breeders can focus on using data to create better varieties,
              faster.
            </Typography>
          </EBSAnimate>
          <EBSAnimate animation="transition.expandIn">
            <div className="mx-auto flex justify-center mt-6 md:mt-10">
              <img
                className="w-224 h-224 md:h-224 md:w-224 lg:w-320 lg:h-320"
                src={Domains}
                alt="logo"
              />
            </div>
          </EBSAnimate>
        </div>
      </div>
    </div>
  );
}

export default memo(LoginView);
