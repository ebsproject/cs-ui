import React, { useEffect } from "react";
import { useDispatch, useSelector, useStore } from "react-redux";
import { useHistory, Redirect } from "react-router-dom";
import queryString from "query-string";
import EBSSplashScreen from "components/atoms/EBSSplashScreen";
import { showMessage } from "store/ducks/message";
import { getAuthToken } from "store/ducks/auth";

export default function CallbackView() {
  const { isSignIn, error } = useSelector(({ auth }) => auth);
  const dispatch = useDispatch();
  const { getState } = useStore();
  const history = useHistory();

  useEffect(() => {
    // * obtain Authentication code
    const authResult = queryString.parse(window.location.search);
    if (authResult.code) {
      // * Obtain id token
      getAuthToken(authResult.code)(dispatch, getState);
    } else {
      dispatch(
        showMessage({
          message: `Authentication code is null`,
          variant: "error",
          autoHideDuration: 10000,
        })
      );
      history.push("/login");
    }
  }, []);

  if (isSignIn) {
    return <Redirect to="/dashboard" />;
  } else if (error) {
    return <Redirect to="/login" />;
  } else {
    return <EBSSplashScreen />;
  }
}
