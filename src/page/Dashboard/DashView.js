import React, { useEffect, memo } from "react";
import { FormattedMessage } from "react-intl";
import { useSelector, useDispatch, useStore } from "react-redux";
import { Core } from "@ebs/styleguide";
import {
  CardList as TenantList,
  Info as TenantInfo,
  InstanceList as TenantInstance,
} from "components/molecule/Tenant";
import Progress from "components/atoms/Progress";
import Products from "components/molecule/ProductCardList";
import { setUserData } from "store/ducks/user";
import EbsLogo from "assets/images/logos/EBS.png";
const { Typography, Card, CardContent, CardHeader, CardMedia } = Core;

function DashView() {
  const { userState } = useSelector(({ auth }) => auth);
  const dispatch = useDispatch();
  const { getState } = useStore();
  const { userInfo, loading } = useSelector(({ user }) => user);
  const { tenantInfo } = useSelector(({ tenant }) => tenant);
  useEffect(() => {
    userInfo === null && !loading && setUserData(userState)(dispatch, getState);
  }, [userInfo]);

  if (!userInfo) return <Progress />;

  const { person: personInfo } = userInfo.contact;

  return (
    <div className="flex flex-row flex-wrap justify-center" data-testid={"dashTestId"}>
      <div className="grid grid-cols-1 gap-4">
        {/* TODO: review Card content */}
        <Card>
          <CardHeader
            title={
              <Typography
                variant="h5"
                color="inherit"
                paragraph={true}
                className="font-ebs text-ebs-green-900 text-center"
              >
                <FormattedMessage
                  id="tnt.comp.comsoon.welcome"
                  defaultMessage={`Welcome ${personInfo.familyName}, ${personInfo.givenName} to Enterprise Breeding System (EBS)`}
                />
              </Typography>
            }
          />
          <CardMedia className="flex flex-row justify-center flex-wrap">
            <div>
              <img src={EbsLogo} style={{ height: "80px" }} />
            </div>
          </CardMedia>
          <CardContent className="grid grid-cols-1 w-full flex-wrap flex-col justify-center justify-items-center">
            <TenantList />
            <TenantInfo />
          </CardContent>
        </Card>
        <Card
          className={`${
            tenantInfo && tenantInfo.instances.length < 2 && "hidden"
          }`}
        >
          <CardContent>
            <TenantInstance />
          </CardContent>
        </Card>
        <Card>
          <CardContent>
            <Products />
          </CardContent>
        </Card>
      </div>
    </div>
  );
}

export default memo(DashView);
