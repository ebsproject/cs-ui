import Dash from "./DashView";
import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";

afterEach(cleanup);

test("Dashboard is in the DOM", async () => {
  const { getByTestId } = render(<Dash />, { wrapper: Wrapper });
  expect(getByTestId("dashTestId")).toBeInTheDocument();
});