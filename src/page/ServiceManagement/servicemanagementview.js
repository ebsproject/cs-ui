import React, { lazy, Suspense, memo } from "react";

const ServiceManagement = lazy(() =>
  System.import("@ebs/sm").then((module) => ({ default: module.App }))
);

function ServiceManagementView({}) {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <ServiceManagement />
    </Suspense>
  );
}

export default memo(ServiceManagementView);
