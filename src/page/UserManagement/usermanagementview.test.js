import UserManagement from "./usermanagementview";
import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";

afterEach(cleanup);

test("UserManagement tabs are in the DOM", async () => {
  const { getByTestId } = render(<UserManagement />, { wrapper: Wrapper });
  expect(getByTestId("userManagementTestid")).toBeInTheDocument();
});
