import React, { memo } from "react";
import Users from "components/organism/Users";
import Roles from "components/organism/Roles";
import { EbsTabsLayout } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
function UserManagementView({}) {
  /* This will be rendered in View
  @prop data-testid: Id to use inside usermanagement.test.js file.
 */
  return (
    <EbsTabsLayout
      data-testid="userManagementTestid"
      tabs={[
        {
          label: <FormattedMessage id="none" defaultMessage="Users" />,
          component: <Users />,
        },
        {
          label: <FormattedMessage id="none" defaultMessage="Roles" />,
          component: <Roles />,
        },
      ]}
    />
  );
}
export default memo(UserManagementView);
// Type and required properties
UserManagementView.propTypes = {};
// Default properties
UserManagementView.defaultProps = {};
