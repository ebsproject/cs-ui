import React, { memo } from "react";
import { EbsTabsLayout } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import PrintoutReports from "components/organism/PrintoutReports";
function SettingsManagementView() {
  return (
    <EbsTabsLayout
      data-testid={"settingsManagementTestId"}
      tabs={[
        {
          label: <FormattedMessage id="none" defaultMessage="Printout" />,
          component: <PrintoutReports />,
        },
      ]}
    />
  );
}
// Type and required properties
SettingsManagementView.propTypes = {};
// Default properties
SettingsManagementView.defaultProps = {};

export default memo(SettingsManagementView);
