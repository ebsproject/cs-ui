import React, { memo } from "react";
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import { useSelector } from "react-redux";
import { Login } from "page/Login";
import { Dashboard } from "page/Dashboard";
import Settings from "page/SettingsManagement";
import { TenantManagement } from "page/TenantManagement";
import ContactRelationshipManagement from "page/CRM";
import BreedingAnalytics from "page/BreedingAnalytics";
import BaseUI from "page/BaseUI";
import ServiceManagement from "page/ServiceManagement";
import UserManagement from "page/UserManagement";
import { Callback } from "page/Callback";
import MainLayout from "layout/MainLayout";
import BlankLayout from "layout/Layout_Blank";
import { NotFound } from "page/NotFound";
import { PrivateRoute } from "auth/PrivateRoute";
import {
  BASE_PATH,
  BASE_UI,
  BREEDING_ANALYTICS,
  CALLBACK,
  CORE_SYSTEM,
  CRM,
  DASHBOARD,
  ERROR_404,
  LOGIN,
  SERVICE_MANAGEMENT,
  SETTINGS,
  TENANT_MANAGEMENT,
  USER_MANAGEMENT,
} from "./paths";

const Routes = () => {
  const { isSignIn } = useSelector(({ auth }) => auth);

  return (
    <Router>
      <Switch>
        <Route exact path={LOGIN} render={(props) => <Login {...props} />} />
        <Route
          exact
          path={CALLBACK}
          render={(props) => <Callback {...props} />}
        />
        <Route exact path={BASE_PATH}>
          <Redirect to={isSignIn ? DASHBOARD : LOGIN} />
        </Route>
        <PrivateRoute
          exact
          path={DASHBOARD}
          render={(props) => <BlankLayout component={Dashboard} {...props} />}
        />
        <PrivateRoute
          exact
          path={CORE_SYSTEM}
          render={() => <Redirect to={TENANT_MANAGEMENT} />}
        />
        <PrivateRoute
          exact
          path={TENANT_MANAGEMENT}
          render={(props) => (
            <MainLayout component={TenantManagement} {...props} />
          )}
        />
        <PrivateRoute
          exact
          path={USER_MANAGEMENT}
          render={(props) => (
            <MainLayout component={UserManagement} {...props} />
          )}
        />
        <PrivateRoute
          exact
          path={CRM}
          render={(props) => (
            <MainLayout component={ContactRelationshipManagement} {...props} />
          )}
        />
        <PrivateRoute
          exact
          path={SETTINGS}
          render={(props) => <MainLayout component={Settings} {...props} />}
        />
        <PrivateRoute
          path={BREEDING_ANALYTICS}
          render={(props) => (
            <MainLayout component={BreedingAnalytics} {...props} />
          )}
        />
        <PrivateRoute
          path={BASE_UI}
          render={(props) => <MainLayout component={BaseUI} {...props} />}
        />
        <PrivateRoute
          path={SERVICE_MANAGEMENT}
          render={(props) => (
            <MainLayout component={ServiceManagement} {...props} />
          )}
        />
        <Route
          exact
          path={ERROR_404}
          render={(props) => <NotFound {...props} />}
        />
        <Redirect to={ERROR_404} />
      </Switch>
    </Router>
  );
};

export default memo(Routes);
