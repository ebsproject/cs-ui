import { client } from "utils/apollo/apollo";
import { FIND_USER_LIST } from "utils/apollo/gql/user";
import { showMessage } from "store/ducks/message";
import Cookies from "js-cookie";
import { signOut } from "store/ducks/auth";
// constants
const initialState = {
  loading: false,
  userInfo: null,
  error: null,
};

const USER_GET_USER = "[USER] GET_USER";
const USER_SET_USER = "[USER] SET_USER";
const USER_ERROR = "[USER] ERROR";

const getUser = () => ({
  type: USER_GET_USER,
});

const setUser = (userInfo) => ({
  type: USER_SET_USER,
  payload: userInfo,
});

const userError = (message) => ({
  type: USER_ERROR,
  payload: message,
});

// reducer
export default function userReducer(state = initialState, { type, payload }) {
  switch (type) {
    case USER_GET_USER:
      return {
        ...state,
        loading: true,
      };
    case USER_SET_USER:
      return {
        ...state,
        userInfo: payload,
        loading: false,
      };
    case USER_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
}
/*

*/
export const setUserData = (userState) => async (dispatch, getState) => {
  const userName =
    userState["http://wso2.org/claims/emailaddress"].toLowerCase();
  dispatch(getUser());
  client
    .query({
      query: FIND_USER_LIST,
      variables: {
        page: { number: 1, size: 10 },
        filters: [{ col: "userName", mod: "EQ", val: userName }],
      },
    })
    .then(({ data }) => {
      if (data.findUserList.content.length > 0) {
        // * Set user id in auth State
        dispatch(setUser(data.findUserList.content[0]));
        let newCookie = JSON.parse(Cookies.get("auth"));
        newCookie.userState["userId"] = data.findUserList.content[0].id;
        Cookies.set("auth", JSON.stringify(newCookie), { sameSite: "Lax" });
      } else {
        dispatch(userError("No user found"));
        signOut()(dispatch, getState);
        dispatch(
          showMessage({
            message: "User not found",
            variant: "warning",
            autoHideDuration: 10000,
          })
        );
      }
    })
    .catch(({ message }) => {
      dispatch(userError(message));
      dispatch(
        showMessage({
          message: `${message}`,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
};
