import jwtService from "services/jwtService";

export const initialState = {
  isLoading: false,
  isAuthenticated: !!localStorage.getItem("id_token"),
  error: null,
};

export const START_LOGIN = "[LOGIN] START LOGIN";
export const LOGIN_SUCCESS = "[LOGIN] LOGIN SUCCESS";
export const LOGIN_FAILURE = "[LOGIN] LOGIN FAILURE";
export const RESET_ERROR = "[LOGIN] RESET ERROR";
export const LOGIN_USER = "[LOGIN] LOGIN USER";
export const SIGN_OUT_SUCCESS = "[LOGIN] SIGN OUT SUCCESS";
export const USER_LOGGED_OUT = "[LOGOUT] LOGGED OUT";

export const startLogin = () => ({
  type: START_LOGIN,
});

export const loginSuccess = () => ({
  type: LOGIN_SUCCESS,
});

export const loginFailure = () => ({
  type: LOGIN_FAILURE,
});

export const resetError = () => ({
  type: RESET_ERROR,
});

export const loginUser = () => (dispatch) => {
  //TODO: Register store in local storage
  dispatch(startLogin());

  let url = `${process.env.REACT_APP_AUTH_CONFIG_AUTH_URL}?client_id=${process.env.REACT_APP_AUTH_CONFIG_CLIENT_ID}&scope=openid&`;
  const redirectUrl = `redirect_uri=${process.env.REACT_APP_AUTH_CONFIG_CALLBACK_URL}&response_type=code`;
  url += redirectUrl;

  window.location.href = url; //redirect to identity server
};

export const signOutSuccess = () => ({
  type: SIGN_OUT_SUCCESS,
});

export const signOut = () => (dispatch) => {
  localStorage.removeItem("id_token");
  dispatch(signOutSuccess());
};

/**
 * Logout
 */
export function logoutUser() {
  return (dispatch, getState) => {
    jwtService.logout();

    window.location.href = "/";

    return dispatch({
      type: USER_LOGGED_OUT,
    });
  };
}

export default function LoginReducer(state = initialState, { type, payload }) {
  switch (type) {
    case START_LOGIN:
      return {
        ...state,
        isLoading: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: true,
        error: null,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: true,
      };
    case RESET_ERROR:
      return {
        error: false,
      };
    case USER_LOGGED_OUT:
      return {
        ...state,
        isAuthenticated: false,
      };
    default:
      return state;
  }
}
