import Client from "utils/apollo";
import { showMessage } from "store/ducks/message";
import {
  FIND_CONTACT_TYPE_LIST,
  FIND_COUNTRY_LIST,
  CREATE_CONTACT,
  MODIFY_CONTACT,
  DELETE_CONTACT,
  DELETE_CONTACT_INFO,
  DELETE_ADDRESS,
} from "utils/apollo/gql/crm";
/*
 Initial state and properties
 */
export const initialState = {
  error: null,
  loading: false,
  contactTypeList: null,
  countryList: null,
  success: false,
};
/*
 Action types
 */
const ERROR = "[CRM] ERROR";
const GET_CONTACT_TYPE_LIST = "[CRM] GET_CONTACT_TYPE_LIST";
const SET_CONTACT_TYPE_LIST = "[CRM] SET_CONTACT_TYPE_LIST";
const GET_COUNTRY_LIST = "[CRM] GET_COUNTRY_LIST";
const SET_COUNTRY_LIST = "[CRM] SET_COUNTRY_LIST";
const MUTATION_CREATE_CONTACT_START = "[CRM] MUTATION_CREATE_CONTACT_START";
const MUTATION_CREATE_CONTACT_SUCCESS = "[CRM] MUTATION_CREATE_CONTACT_SUCCESS";
const MUTATION_MODIFY_CONTACT_START = "[CRM] MUTATION_MODIFY_CONTACT_START";
const MUTATION_MODIFY_CONTACT_SUCCESS = "[CRM] MUTATION_MODIFY_CONTACT_SUCCESS";
const MUTATION_DELETE_CONTACT_START = "[CRM] MUTATION_DELETE_CONTACT_START";
const MUTATION_DELETE_CONTACT_SUCCESS = "[CRM] MUTATION_DELETE_CONTACT_SUCCESS";
const CLEAN_CONTACT_SUCCESS = "[CRM] CLEAN_CONTACT_SUCCESS";
/*
 Arrow function for change state
 */
const getContactTypeList = () => ({
  type: GET_CONTACT_TYPE_LIST,
});

const setContactTypeList = (list) => ({
  type: SET_CONTACT_TYPE_LIST,
  payload: list,
});

const getCountryList = () => ({
  type: GET_COUNTRY_LIST,
});

const setCountryList = (list) => ({
  type: SET_COUNTRY_LIST,
  payload: list,
});

const createContactStart = () => ({
  type: MUTATION_CREATE_CONTACT_START,
});

const createContactSuccess = () => ({
  type: MUTATION_CREATE_CONTACT_SUCCESS,
});

const modifyContactStart = () => ({
  type: MUTATION_MODIFY_CONTACT_START,
});

const modifyContactSuccess = () => ({
  type: MUTATION_MODIFY_CONTACT_SUCCESS,
});

const deleteContactStart = () => ({
  type: MUTATION_DELETE_CONTACT_START,
});

const deleteContactSuccess = () => ({
  type: MUTATION_DELETE_CONTACT_SUCCESS,
});

export const cleanContactSuccess = () => ({
  type: CLEAN_CONTACT_SUCCESS,
});

const crmFailure = (message) => ({
  type: ERROR,
  payload: message,
});
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case GET_CONTACT_TYPE_LIST:
      return {
        ...state,
        loading: true,
      };
    case SET_CONTACT_TYPE_LIST:
      return {
        ...state,
        contactTypeList: payload,
        loading: false,
      };
    case GET_COUNTRY_LIST:
      return {
        ...state,
        loading: true,
      };
    case SET_COUNTRY_LIST:
      return {
        ...state,
        countryList: payload,
        loading: false,
      };
    case MUTATION_CREATE_CONTACT_START:
      return {
        ...state,
        loading: true,
      };
    case MUTATION_CREATE_CONTACT_SUCCESS:
      return {
        ...state,
        success: true,
        loading: false,
      };
    case MUTATION_MODIFY_CONTACT_START:
      return {
        ...state,
        loading: true,
      };
    case MUTATION_MODIFY_CONTACT_SUCCESS:
      return {
        ...state,
        success: true,
        loading: false,
      };
    case MUTATION_DELETE_CONTACT_START:
      return {
        ...state,
        loading: true,
      };
    case MUTATION_DELETE_CONTACT_SUCCESS:
      return {
        ...state,
        success: true,
        loading: false,
      };
    case CLEAN_CONTACT_SUCCESS:
      return {
        ...state,
        success: false,
      };
    case ERROR:
      return {
        ...state,
        loading: false,
        error: [...state.error, payload],
      };
    default:
      return state;
  }
}
/*

*/
export const findContactTypeList = () => async (dispatch, getState) => {
  dispatch(getContactTypeList());
  Client.query({
    query: FIND_CONTACT_TYPE_LIST,
    variables: {
      page: { number: 1, size: 10 },
    },
  })
    .then(({ data }) => {
      dispatch(setContactTypeList(data.findContactTypeList.content));
    })
    .catch(({ message }) => {
      dispatch(crmFailure(message));
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
};
/*

*/
export const findCountryList = () => async (dispatch, getState) => {
  dispatch(getCountryList());
  Client.query({
    query: FIND_COUNTRY_LIST,
    variables: {
      page: { number: 1, size: 300 },
    },
  })
    .then(({ data }) => {
      dispatch(setCountryList(data.findCountryList.content));
    })
    .catch(({ message }) => {
      dispatch(crmFailure(message));
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
};
/*

*/
export const createContact = (contactInput) => async (dispatch, getState) => {
  dispatch(createContactStart());
  try {
    const { errors, data } = await Client.mutate({
      mutation: CREATE_CONTACT,
      variables: { contact: contactInput },
    });
    errors && dispatch(crmFailure(errors[0].message));
    errors &&
      dispatch(
        showMessage({
          message: errors[0].message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    data && dispatch(createContactSuccess());
    data &&
      dispatch(
        showMessage({
          message: "Contact created successfully",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
  } catch ({ message }) {
    dispatch(crmFailure(message));
    dispatch(
      showMessage({
        message: message,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } finally {
    dispatch(cleanContactSuccess());
  }
};

/*
 */
export const modifyContact =
  (contactInput, other) => async (dispatch, getState) => {
    dispatch(modifyContactStart());
    const { address, phones } = other;
    try {
      address.map((data) => {
        Client.mutate({
          mutation: DELETE_ADDRESS,
          variables: { addressId: data.id },
        });
      });
      phones.map((data) => {
        Client.mutate({
          mutation: DELETE_CONTACT_INFO,
          variables: { contactInfoId: data.id },
        });
      });
      const { errors } = await Client.mutate({
        mutation: MODIFY_CONTACT,
        variables: { contact: contactInput },
      });
      dispatch(errors ? crmFailure(errors[0].message) : modifyContactSuccess());
      dispatch(
        showMessage({
          message: errors?.[0]?.message || "Contact modified successfully",
          variant: errors ? "error" : "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch ({ message }) {
      dispatch(crmFailure(message));
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(cleanContactSuccess());
    }
  };

export const deleteContact = (contactId) => async (dispatch, getState) => {
  dispatch(deleteContactStart());
  try {
    const { data } = await Client.mutate({
      mutation: DELETE_CONTACT,
      variables: { contactId: contactId },
    });
    data && dispatch(deleteContactSuccess());
    data &&
      dispatch(
        showMessage({
          message: "Contact deleted successfully",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
  } catch ({ message }) {
    dispatch(crmFailure(message));
    dispatch(
      showMessage({
        message: message,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } finally {
    dispatch(cleanContactSuccess());
  }
};
