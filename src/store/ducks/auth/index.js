import axios from "axios";
import oauth from "axios-oauth-client";
import { showMessage } from "store/ducks/message";
import jwtDecode from "jwt-decode";
import { setUserData } from "store/ducks/user";
import { setTenantContext } from "store/ducks/tenant";
import Cookies from "js-cookie";

const initialState = {
  loading: false,
  error: null,
  isSignIn: Cookies.get("auth") ? true : false,
  userState:
    (Cookies.get("auth") && JSON.parse(Cookies.get("auth")).userState) || null,
  idToken:
    (Cookies.get("auth") && JSON.parse(Cookies.get("auth")).idToken) || null,
  expireAt:
    (Cookies.get("auth") &&
      new Date(JSON.parse(Cookies.get("auth")).expireAt)) ||
    null,
  refreshIdToken:
    (Cookies.get("auth") && JSON.parse(Cookies.get("auth"))?.refreshIdToken) ||
    null,
};

// constants
const SING_IN_LOADING = "[AUTH] SING_IN_LOADING";
const SING_IN_ERROR = "[AUTH] SING_IN_ERROR";
const SIGN_IN_SUCCESS = "[AUTH] SIGN_IN_SUCCESS";
const REFRESH_TOKEN_SUCCESS = "[AUTH] REFRESH_TOKEN_SUCCESS";
const REFRESH_TOKEN_ERROR = "[AUTH] REFRESH_TOKEN_ERROR";
const SIGN_OUT = "[AUTH] SIGN_OUT";

// Arrow functions
export const signIn = () => ({
  type: SING_IN_LOADING,
});

export const signInSuccess = (userState) => ({
  type: SIGN_IN_SUCCESS,
  payload: userState,
});

export const signInError = (message) => ({
  type: SING_IN_ERROR,
  payload: message,
});

const updateToken = (tokenData) => ({
  type: REFRESH_TOKEN_SUCCESS,
  payload: tokenData,
});

const refreshIdTokenError = (message) => ({
  type: REFRESH_TOKEN_ERROR,
  payload: message,
});

const signSessionOut = () => ({
  type: SIGN_OUT,
});

// reducer
export default function authReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SING_IN_LOADING:
      return {
        ...state,
        loading: true,
      };
    case SING_IN_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        loading: false,
        isSignIn: true,
        error: null,
        ...payload,
      };
    case REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        idToken: payload.idToken,
        expireAt: payload.expireAt,
        refreshIdToken: payload.refreshIdToken,
        error: null,
      };
    case REFRESH_TOKEN_ERROR:
      return {
        ...state,
        idToken: null,
        expireAt: null,
        refreshIdToken: null,
        isSignIn: false,
        error: payload,
      };
    case SIGN_OUT:
      return {
        isSignIn: false,
      };
    default:
      return state;
  }
}

export const signOut = () => (dispatch, getState) => {
  dispatch(signSessionOut());
  Cookies.remove("auth");
  localStorage.clear();
};

export const signOutByInactivity = () => (dispatch, getState) => {
  dispatch(signOut());
  dispatch(
    showMessage({
      message: "Your session was closed by inactivity",
      variant: "warning",
      anchorOrigin: {
        vertical: "top",
        horizontal: "right",
      },
    })
  );
};

export const getAuthToken = (authCode) => async (dispatch, getState) => {
  const Client = oauth.client(axios.create(), {
    url: process.env.REACT_APP_AUTH_CONFIG_TOKEN_URL,
    grant_type: "authorization_code",
    client_id: process.env.REACT_APP_AUTH_CONFIG_CLIENT_ID,
    client_secret: process.env.REACT_APP_AUTH_CONFIG_CLIENT_SECRET,
    redirect_uri: process.env.REACT_APP_AUTH_CONFIG_CALLBACK_URL,
    code: authCode,
  });
  Client()
    .then(({ expires_in, id_token, refresh_token }) => {
      const userState = jwtDecode(id_token);
      localStorage.setItem("id_token", id_token);
      let tokenExpire = new Date(Date.now() + expires_in * 1000);
      Cookies.set(
        "auth",
        JSON.stringify({
          refreshIdToken: refresh_token,
          idToken: id_token,
          expireAt: tokenExpire,
          userState: {
            "http://wso2.org/claims/emailaddress":
              userState["http://wso2.org/claims/emailaddress"],
          },
          context: {
            tenantId: null,
            instanceId: null,
            domainId: null,
          },
        }),
        { sameSite: "Lax" }
      );
      // * Save User Profile to redux
      setUserData(userState)(dispatch, getState);
      dispatch(
        signInSuccess({
          userState: userState,
          idToken: id_token,
          expireAt: tokenExpire,
          refreshIdToken: refresh_token,
        })
      );
    })
    .catch(({ message }) => {
      dispatch(signInError(message));
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "center",
          },
        })
      );
    });
};

export const refreshToken = (refreshToken) => async (dispatch, getState) => {
  const Client = oauth.client(axios.create(), {
    url: process.env.REACT_APP_AUTH_CONFIG_TOKEN_URL,
    grant_type: "refresh_token",
    client_id: process.env.REACT_APP_AUTH_CONFIG_CLIENT_ID,
    client_secret: process.env.REACT_APP_AUTH_CONFIG_CLIENT_SECRET,
    refresh_token: refreshToken,
  });
  Client()
    .then(({ expires_in, id_token, refresh_token }) => {
      localStorage.setItem("id_token", id_token);
      let tokenExpire = new Date(Date.now() + expires_in * 1000);
      Cookies.set(
        "auth",
        JSON.stringify({
          refreshIdToken: refresh_token,
          idToken: id_token,
          expireAt: tokenExpire,
          userState: JSON.parse(Cookies.get("auth"))?.userState,
          context: JSON.parse(Cookies.get("auth"))?.context,
        }),
        { sameSite: "Lax" }
      );
      dispatch(
        updateToken({
          idToken: id_token,
          expireAt: tokenExpire,
          refreshIdToken: refresh_token,
        })
      );
    })
    .catch(({ message }) => {
      signOut()(dispatch, getState);
      dispatch(refreshIdTokenError(message));
      dispatch(
        showMessage({
          message: `Internal Server Error: ${message}`,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
};
