import { combineReducers } from "redux";
import tenant from "store/ducks/tenant";
import login from "store/ducks/login";
import auth from "store/ducks/auth";
import user from "store/ducks/user";
import message from "store/ducks/message";
import crm from "store/ducks/crm";

export default combineReducers({
  auth,
  crm,
  login,
  tenant,
  user,
  message,
});
