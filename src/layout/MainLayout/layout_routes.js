import React from "react";
import MainLayout from "./layout";
import { ApolloProvider } from "@apollo/client";
import client from "utils/apollo";

const LayoutRoutes = ({ component: Component, ...rest }) => {
  return (
    <ApolloProvider client={client}>
      <MainLayout>
        <Component {...rest} />
      </MainLayout>
    </ApolloProvider>
  );
};

export default LayoutRoutes;
