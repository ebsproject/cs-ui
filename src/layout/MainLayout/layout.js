import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import clsx from "clsx";
import { Core, Styles, Icons } from "@ebs/styleguide";
const { makeStyles } = Styles;
const {
  AppBar,
  Drawer,
  Toolbar,
  List,
  CssBaseline,
  Typography,
  Divider,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Icon,
  Collapse,
} = Core;
const {
  ChevronLeft,
  ChevronRight,
  Dashboard,
  ExpandLess,
  ExpandMore,
  FilterList,
  Help,
  LensRounded,
  Search,
  Apps,
  Menu,
} = Icons;
import DomainMenus from "components/molecule/MenuDomainList";
import { FormattedMessage } from "react-intl";
import AvatarProfile from "components/atoms/AvatarProfile";
import EbsLogo from "components/atoms/EBSLogoVertical";
import UserMessage from "components/atoms/Message";
import Footer from "components/molecule/Footer";
import {
  findDomain,
  findProductList,
  findInstance,
  findTenant,
} from "store/ducks/tenant";
import Progress from "components/atoms/Progress";
import { useDispatch, useSelector, useStore } from "react-redux";
import { setUserData } from "store/ducks/user";

// Import all Icons
const iconList = require.context("assets/images/domains", true, /\.svg$/);
const icons = iconList.keys().reduce((images, path) => {
  images[path] = iconList(path);
  return images;
}, {});

const drawerWidth = 240;
const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  actions: {
    flexGrow: 1,
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("md")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
      display: "block",
    },
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  logo: {
    marginLeft: 0,
    width: 250,
    height: 92,
  },
  content: {
    flexGrow: 1,
    overflow: "auto",
    padding: theme.spacing(2),
  },
  footer: {
    position: "fixed",
    bottom: 0,
  },
}));

const MainLayout = React.forwardRef(({ children, ...rest }, ref) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { getState } = useStore();
  const [open, setOpen] = useState(true);
  const { productList, domainInfo, tenantContext, instanceInfo, tenantInfo } =
    useSelector(({ tenant }) => tenant);
  const { userState } = useSelector(({ auth }) => auth);
  const { userInfo } = useSelector(({ user }) => user);
  const [openApps, setOpenApps] = useState(false);
  const history = useHistory();

  useEffect(() => {
    !userInfo && setUserData(userState)(dispatch, getState);
    findDomain(tenantContext.domainId)(dispatch, getState);
    !instanceInfo && findInstance(tenantContext.instanceId)(dispatch, getState);
  }, [
    userInfo,
    domainInfo,
    tenantContext.domainId,
    instanceInfo,
    tenantContext.instanceId,
  ]);
  useEffect(() => {
    !tenantInfo && findTenant(tenantContext.tenantId)(dispatch, getState);
  }, [tenantInfo, tenantContext.tenantId]);

  useEffect(() => {
    findProductList(tenantContext.domainId)(dispatch, getState);
  }, [productList, tenantContext.domainId]);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleClick = (path) => {
    history.push(path);
  };

  const handleAppsClick = () => {
    setOpenApps(!openApps);
  };

  if (!productList) {
    return <Progress />;
  } else {
    return (
      <div className="flex">
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open,
          })}
        >
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}
            >
              <Menu />
            </IconButton>
            <Icon color="inherit" aria-label="open drawer" edge="start">
              <img src={icons[`./${domainInfo?.icon}`]} />
            </Icon>
            <Typography variant="h6" noWrap>
              <FormattedMessage
                id="none"
                defaultMessage={domainInfo?.name}
              />
            </Typography>
            <div className={classes.actions}>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                edge="start"
                href="/dashboard"
              >
                <Dashboard />
              </IconButton>
            </div>
            <IconButton
              aria-label="show 17 new notifications"
              color="inherit"
              edge="end"
            >
              <Search />
            </IconButton>
            <IconButton
              aria-label="show 17 new notifications"
              color="inherit"
              edge="end"
            >
              <Help />
            </IconButton>
            <AvatarProfile />
          </Toolbar>
        </AppBar>
        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div className={classes.toolbar}>
            <EbsLogo className={classes.logo} />
          </div>
          <List>
            {productList
              ?.slice()
              .sort(function (a, b) {
                return a.menuOrder - b.menuOrder;
              })
              .map((item, index) => (
                <ListItem
                  button
                  key={item.id}
                  onClick={() => handleClick(item.path)}
                >
                  <ListItemIcon>
                    {item.icon && (
                      <Icon
                        className="list-item-icon text-16 flex-shrink-0"
                        color="action"
                      >
                        {item.icon}
                      </Icon>
                    )}

                    {item.icon === null ||
                      (item.icon === "" && <LensRounded />)}
                  </ListItemIcon>
                  <ListItemText primary={`${item.name}`} />
                </ListItem>
              ))}
          </List>
          <Divider />
          <List>
            <ListItem button onClick={handleAppsClick}>
              <ListItemIcon>
                <Apps />
              </ListItemIcon>
              <ListItemText
                className={classes.listText}
                primary="Shortcut Domains"
              />

              {openApps ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={openApps} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <DomainMenus />
              </List>
            </Collapse>
          </List>
          <div className={classes.toolbar}>
            <IconButton onClick={open ? handleDrawerClose : handleDrawerOpen}>
              {open ? <ChevronLeft /> : <ChevronRight />}
            </IconButton>
          </div>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {children}
          <UserMessage />
          <div className={classes.footer}>
            <Footer />
          </div>
        </main>
      </div>
    );
  }
});

export default MainLayout;
