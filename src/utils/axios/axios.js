import axios from "axios";

import { configure } from "axios-hooks";
import LRU from "lru-cache";

export const client = axios.create({
  baseURL: process.env.REACT_APP_CSAPI_URI_REST,
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

client.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${localStorage.getItem("id_token")}`;
  return config;
});

const cache = new LRU({ max: 10 });
configure({ client, cache });

export const printoutClient = axios.create({
  baseURL: process.env.REACT_APP_PRINTOUT_ENDPOINT,
  headers: {
    "Content-Type": "application/json",
  },
});

printoutClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${localStorage.getItem("id_token")}`;
  return config;
});
