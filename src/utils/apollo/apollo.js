import fetch from "cross-fetch";
import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  ApolloLink,
  from,
} from "@apollo/client";

const httpLink = new HttpLink({
  uri: process.env.REACT_APP_CSAPI_URI_GRAPHQL,
  fetch,
});

const authLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers }) => ({
    headers: {
      ...headers,
      authorization: `Bearer ${localStorage.getItem("id_token")}`,
    },
  }));
  return forward(operation);
});

export const client = new ApolloClient({
  link: from([authLink, httpLink]),
  cache: new InMemoryCache({
    addTypename: false,
  }),
});
