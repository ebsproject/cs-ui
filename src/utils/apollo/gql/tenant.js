import gql from "graphql-tag";

// * UTILITIES
// * Find ${entity} List
export const QUERY_ROWS = (entity, content) => gql`
  query GetRows($size: Int!,$number: Int!){
    find${entity}List(page: { size: $size, number: $number }) {
      ${content}
    }
  }
`;
// * QUERIES
// * FIND DOMAIN
export const FIND_DOMAIN = gql`
  query findDomain($id: ID!) {
    findDomain(id: $id) {
      id
      name
      icon
      products {
        id
        name
        icon
        mainEntity
        menuOrder
        path
        description
        help
        productfunctions {
          id
          action
          description
        }
      }
    }
  }
`;
// * FIND_PRODUCT_LIST
export const FIND_PRODUCT_LIST = gql`
  query findProductList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findProductList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        description
        path
        help
        icon
        menuOrder
      }
    }
  }
`;
// * Find Program List
export const FIND_PROGRAM_LIST = gql`
  query findProgramList($tenantId: String!) {
    findProgramList(filters: [{ mod: EQ, col: "tenant.id", val: $tenantId }]) {
      content {
        id
        name
      }
    }
  }
`;
// *FIND TENANT USED BY MODIFY TENANT to avoid executing all query again
export const FIND_TENANT_ID = gql`
  query findTenant($id: ID!) {
    findTenant(id: $id) {
      instances {
        domaininstances {
          id
          context
          sgContext
          domain {
            id
            core
            name
          }
        }
      }
    }
  }
`;
// * Find tenant
export const FIND_TENANT = gql`
  query findTenant($id: ID!) {
    findTenant(id: $id) {
      id
      name
      expiration
      instances {
        id
        name
        domaininstances {
          domain {
            products {
              domain {
                name
              }
              id
              name
            }
          }
        }
      }
      organization {
        id
        name
        logo
        slogan
        webPage
        legalName
      }
      customer {
        id
        name
        officialEmail
        logo
        phone
      }
    }
  }
`;
// * Find instance
export const FIND_INSTANCE = gql`
  query findInstance($id: ID!) {
    findInstance(id: $id) {
      id
      name
      domaininstances {
        id
        context
        sgContext
        mfe
        domain {
          id
          name
          info
          icon
          prefix
        }
      }
    }
  }
`;
// * Find Entity reference list
// ! @deprecated
export const FIND_ENTITY_REFERENCE_LIST = gql`
  query findEntityReferenceList($val: String!) {
    findEntityReferenceList(filters: { col: "entity", val: $val }) {
      content {
        id
        entity
        textfield
        valuefield
        attributess {
          id
          name
          description
          attComponent
          isrequired
          ismultiline
          sm
          md
          sortno
          htmltag {
            tagName
          }
        }
      }
    }
  }
`;
// * Find component list
// ! @deprecated
export const FIND_COMPONENT_LIST = gql`
  query findComponentList {
    findComponentList(
      page: { size: 100, number: 1 }
      sort: [{ col: "releaseNo", mod: ASC }]
    ) {
      content {
        id
        name
      }
    }
  }
`;
// * CREATE
// * Create customer
export const CREATE_CUSTOMER = gql`
  mutation createCustomer($type: CustomerInput!) {
    createCustomer(CustomerTo: $type) {
      id
      name
    }
  }
`;
// * Create organization
export const CREATE_ORGANIZATION = gql`
  mutation createOrganization($type: OrganizationInput!) {
    createOrganization(OrganizationTo: $type) {
      id
      name
    }
  }
`;
// * Create Tenant
export const CREATE_TENANT = gql`
  mutation createTenant($type: TenantInput!) {
    createTenant(TenantTo: $type) {
      id
      name
    }
  }
`;
// * Create instance
export const CREATE_INSTANCE = gql`
  mutation createInstance($type: InstanceInput!) {
    createInstance(InstanceTo: $type) {
      id
      name
    }
  }
`;
// * Create domain instance
export const CREATE_DOMAININSTANCE = gql`
  mutation createDomainInstance($type: DomainInstanceInput!) {
    createDomainInstance(DomainInstanceTo: $type) {
      id
    }
  }
`;

// * MODIFY
// * Modify entity
export const MODIFY_ENTITY = (nameentity) => gql`
  mutation modifyEntity($type: ${nameentity}Input!){
    modify${nameentity}(${nameentity}To: $type){
      id
      name
    }
  }
`;

// * DELETE
// * Delete tenant
export const DELETE_TENANT = gql`
  mutation deleteTenant($id: Int!) {
    deleteTenant(tenantId: $id)
  }
`;
