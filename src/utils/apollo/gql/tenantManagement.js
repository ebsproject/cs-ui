import { gql } from "@apollo/client";
// QUERIES
// TODO: Delete findDomain, findProductList
// * FIND DOMAIN
export const FIND_DOMAIN = gql`
  query findDomain($id: ID!) {
    findDomain(id: $id) {
      id
      name
      icon
      products {
        id
        name
        icon
        mainEntity
        menuOrder
        path
        description
        help
        productfunctions {
          id
          action
          description
        }
      }
    }
  }
`;
// * FIND_PRODUCT_LIST
export const FIND_PRODUCT_LIST = gql`
  query findProductList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findProductList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        description
        path
        help
        icon
        menuOrder
      }
    }
  }
`;
// * FIND DOMAINS LIST
export const FIND_DOMAIN_LIST = gql`
  query findDomainList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findDomainList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
      }
    }
  }
`;
// CREATE
// * CREATE NEW PRODUCT
export const CREATE_PRODUCT = gql`
  mutation createProduct($ProductInput: ProductInput!) {
    createProduct(product: $ProductInput) {
      id
    }
  }
`;
// * CREATE DOMAIN INSTANCE
export const CREATE_DOMAIN_INSTANCE = gql`
  mutation createDomainInstance($DomainInstanceTo: DomainInstanceInput!) {
    createDomainInstance(DomainInstanceTo: $DomainInstanceTo) {
      id
    }
  }
`;
// MODIFY
// * MODIFY PRODUCT
export const MODIFY_PRODUCT = gql`
  mutation modifyProduct($ProductInput: ProductInput!) {
    modifyProduct(product: $ProductInput) {
      id
    }
  }
`;
// * MODIFY DOMAIN INSTANCE
export const MODIFY_DOMAIN_INSTANCE = gql`
  mutation modifyDomainInstance($DomainInstance: DomainInstanceInput!) {
    modifyDomainInstance(domainInstance: $DomainInstance) {
      id
    }
  }
`;
// DELETE
// * DELETE PRODUCT
export const DELETE_PRODUCT = gql`
  mutation deleteProduct($productId: Int!) {
    deleteProduct(productId: $productId)
  }
`;
// * DELETE DOMAIN INSTANCE
export const DELETE_DOMAIN_INSTANCE = gql`
  mutation deleteDomainInstance($id: Int!) {
    deleteDomainInstance(domainInstanceId: $id)
  }
`;
