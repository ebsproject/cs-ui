import { gql } from "@apollo/client";

// QUERY FIND LIST

export const FIND_PRINTOUT_TEMPLATE_LIST = gql`
  query findPrintoutTemplateList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findPrintoutTemplateList(page: $page, sort: $sort, filters: $filters) {
      totalPages
      content {
        id
        name
        description
        zpl
        programs {
          id
          name
        }
        products {
          id
          name
          domain {
            name
          }
        }
      }
    }
  }
`;
// CREATE
export const CREATE_PRINTOUT_TEMPLATE = gql`
  mutation createPrintoutTemplate(
    $name: String!
    $description: String!
    $zpl: String
    $tenantId: Int!
  ) {
    createPrintoutTemplate(
      printoutTemplate: {
        id: 0
        name: $name
        description: $description
        zpl: $zpl
        tenantId: $tenantId
      }
    ) {
      id
    }
  }
`;

export const ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS = gql`
  mutation addPrintoutTemplateToPrograms(
    $printoutTemplateId: Int!
    $programIds: [Int!]!
  ) {
    addPrintoutTemplateToPrograms(
      printoutTemplateId: $printoutTemplateId
      programIds: $programIds
    ) {
      id
    }
  }
`;

export const ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS = gql`
  mutation addPrintoutTemplateToProductsI(
    $printoutTemplateId: Int!
    $productIds: [Int!]!
  ) {
    addPrintoutTemplateToProducts(
      printoutTemplateId: $printoutTemplateId
      productIds: $productIds
    ) {
      id
    }
  }
`;

// MODIFY
export const MODIFY_PRINTOUT_TEMPLATE = gql`
  mutation modifyPrintoutTemplate($printoutTemplate: PrintoutTemplateInput!) {
    modifyPrintoutTemplate(printoutTemplate: $printoutTemplate) {
      id
    }
  }
`;

// DELETE
export const DELETE_PRINTOUT_TEMPLATE = gql`
  mutation deletePrintoutTemplate($printoutTemplateId: Int!) {
    deletePrintoutTemplate(printoutTemplateId: $printoutTemplateId)
  }
`;

export const REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS = gql`
  mutation removePrintoutTemplateFromPrograms(
    $printoutTemplateId: Int!
    $programIds: [Int!]!
  ) {
    removePrintoutTemplateFromPrograms(
      printoutTemplateId: $printoutTemplateId
      programIds: $programIds
    ) {
      id
    }
  }
`;

export const REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS = gql`
  mutation removePrintoutTemplateFromProducts(
    $printoutTemplateId: Int!
    $productIds: [Int!]!
  ) {
    removePrintoutTemplateFromProducts(
      printoutTemplateId: $printoutTemplateId
      productIds: $productIds
    ) {
      id
    }
  }
`;
