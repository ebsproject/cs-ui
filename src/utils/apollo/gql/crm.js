import { gql } from "@apollo/client";
// TODO QUERIES
/*
 @graph FIND_CONTACT_LIST
 @param page: Page number and registers
 @param sort: [Column and sort mode]
 @param filters: [Filter mode, column and filter value]
 @param disjunctionFilter: There are more than one filter applied?
*/
export const FIND_CONTACT_LIST = gql`
  query FIND_CONTACT_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        category
        institution {
          commonName
          legalName
          principalContact {
            id
            person {
              familyName
              givenName
            }
          }
        }
        person {
          familyName
          givenName
          additionalName
          jobTitle
        }
      }
    }
  }
`;
/*
 @graph FIND_CONTACT_TYPE_LIST
 @param page: Page number and registers
 @param sort: [Column and sort mode]
 @param filters: [Filter mode, column and filter value]
 @param disjunctionFilter: There are more than one filter applied?
*/
export const FIND_CONTACT_TYPE_LIST = gql`
  query FIND_CONTACT_TYPE_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactTypeList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
        category
      }
    }
  }
`;
/*
@graph FIND_CONTACT
@param id: contact id
*/
export const FIND_CONTACT = gql`
  query FIND_CONTACT($id: ID!) {
    findContact(id: $id) {
      contactTypes {
        id
        name
        category
      }
      addresses {
        id
        location
        region
        zipCode
        streetAddress
        country {
          id
          name
        }
      }
      contactInfos {
        id
        value
        contactInfoType {
          id
        }
      }
    }
  }
`;
/* 
 @graph FIND_COUNTRY_LIST
 @param page: Page number and registers
 @param sort: [Column and sort mode]
 @param filters: [Filter mode, column and filter value]
 @param disjunctionFilter: There are more than one filter applied?
*/
export const FIND_COUNTRY_LIST = gql`
  query FIND_COUNTRY_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findCountryList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
      }
    }
  }
`;
// TODO CREATE
/* 
 @graph CREATE_CONTACT
 @param contact: { 
  * id: Int!, 
  * category: 
  * ContactTypeCategory!, 
  * contactTypeIds: [Int!], 
  * addresses: [AddressesInput!], 
  * ContactInfos: [ContactInfoInput!], 
  * person: PersonInput, 
  * institution: InstitutionInput
  }
*/
export const CREATE_CONTACT = gql`
  mutation CREATE_CONTACT($contact: ContactInput!) {
    createContact(contact: $contact) {
      id
    }
  }
`;
// TODO MODIFY
/*
  @graph MODIFY_CONTACT
  @param contact: { 
  * id: Int!, 
  * category: 
  * ContactTypeCategory!, 
  * contactTypeIds: [Int!], 
  * addresses: [AddressesInput!], 
  * ContactInfos: [ContactInfoInput!], 
  * person: PersonInput, 
  * institution: InstitutionInput
  }
*/
export const MODIFY_CONTACT = gql`
  mutation MODIFY_CONTACT($contact: ContactInput!) {
    modifyContact(contact: $contact) {
      id
    }
  }
`;
// TODO DELETE
/*
@graph DELETE_CONTACT
@param contactId: Contact id
*/
export const DELETE_CONTACT = gql`
  mutation DELETE_CONTACT($contactId: Int!) {
    deleteContact(contactId: $contactId)
  }
`;
/*
@graph DELETE_CONTACT_INFO
@param contactId: Contact Info id
*/
export const DELETE_CONTACT_INFO = gql`
  mutation REMOVE_CONTACT_INFO($contactInfoId: Int!) {
    removeContactInfo(contactInfoId: $contactInfoId)
  }
`;
/*
@graph DELETE_ADDRESS
@param contactId: Address id
*/
export const DELETE_ADDRESS = gql`
  mutation REMOVE_ADDRESS($addressId: Int!) {
    removeAddress(addressId: $addressId)
  }
`;
