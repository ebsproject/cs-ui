import Cookies from "js-cookie";

export const getDomainContext = (domainPrefix) => {
  return JSON.parse(Cookies.get("domainContexts"))[domainPrefix];
};

export const getContext = () => {
  return JSON.parse(Cookies.get("auth")).context;
};

export const getAuthState = () => {
  return JSON.parse(Cookies.get("auth")).userState;
};

export const getTokenId = () => {
  return JSON.parse(Cookies.get("auth")).idToken;
};
