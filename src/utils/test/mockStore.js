import { configureStore } from "@reduxjs/toolkit";
import crmReducer from "store/ducks/crm";
import messageReducer from "store/ducks/message";
import authReducer from "store/ducks/auth";
import tenantReducer from "store/ducks/tenant";
import userReducer from "store/ducks/user";

export const store = configureStore({
  reducer: {
    crm: crmReducer,
    message: messageReducer,
    auth: authReducer,
    tenant: tenantReducer,
    user: userReducer,
  },
  preloadedState: {
    crm: {
      contactTypeList: [{ id: 1, name: "mock", category: "Person" }],
      countryList: [],
    },
    auth: {
      isSignIn: true,
      userState: {
        "http://wso2.org/claims/emailaddress": "test.mail@cgiar.org",
      },
    },
    user: {
      userInfo: { contact: { person: {} },tenants:[{}] },
    },
  },
});
