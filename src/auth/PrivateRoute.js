import React, { useEffect } from "react";
import { Route, Redirect, useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { refreshToken, signOutByInactivity } from "store/ducks/auth";

export const PrivateRoute = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { isSignIn, expireAt, refreshIdToken } = useSelector(
    ({ auth }) => auth
  );
  useEffect(() => {
    !isSignIn && history.push("/login");
  }, [isSignIn]);

  useEffect(() => {
    const refreshMilliseconds = expireAt - Date.now() - 600000;
    if (expireAt - Date.now() < refreshMilliseconds) {
      dispatch(signOutByInactivity());
      history.push("/login");
    }
    const timer = setTimeout(() => {
      !(expireAt - Date.now() < refreshMilliseconds) &&
        dispatch(refreshToken(refreshIdToken));
    }, refreshMilliseconds);
    return () => clearTimeout(timer);
  }, [history.location.pathname]);

  return isSignIn ? <Route {...props} /> : <Redirect to="/login" />;
};
