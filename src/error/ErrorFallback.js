import React from "react";
import { Core, Icons, Styles } from "@ebs/styleguide";
const { Error } = Icons;
const { makeStyles } = Styles;
const { Avatar, CssBaseline, Typography, Container, Button } = Core;

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function ErrorFallback({ error, resetErrorBoundary }) {
  const classes = useStyles();

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className="m-20 bg-red-900">
          <Error />
        </Avatar>
        <Typography component="h1" variant="h5">
          Something went wrong:{" "}
          <span className="text-red-500">{error.message}</span>
        </Typography>
        <Button
          className="bg-red-default text-white m-10 "
          onClick={resetErrorBoundary}
        >
          Try again.
        </Button>
      </div>
    </Container>
  );
}
