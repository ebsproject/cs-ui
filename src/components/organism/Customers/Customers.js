import React from "react";
import PropTypes from "prop-types";
import { Core, Icons } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { EbsGrid } from "@ebs/components";
const { Add } = Icons;
const { IconButton, Typography } = Core;

//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Customers = React.forwardRef(({}, ref) => {
  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Name" />
        </Typography>
      ),
      accessor: "name",
      csvHeader: "Name",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Contact Email" />
        </Typography>
      ),
      accessor: "officialEmail",
      csvHeader: "Contact Mail",
      filter: true,
      width: 350,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Alternate Email" />
        </Typography>
      ),
      accessor: "alternateEmail",
      csvHeader: "Alternate Email",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Title" />
        </Typography>
      ),
      accessor: "jobTitle",
      csvHeader: "Title",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Logo" />
        </Typography>
      ),
      accessor: "logo",
      csvHeader: "Logo",
      filter: true,
      width: 300,
    },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(selectedRows);
          refresh();
        }}
        color="inherit"
      >
        <Add />
      </IconButton>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(JSON.stringify(rowData));
          refresh();
        }}
        color="inherit"
      >
        <Add />
      </IconButton>
    );
  };

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      toolbar={true}
      columns={columns}
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      entity="Customer"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="List" />
        </Typography>
      }
      select="multi"
      csvfilename='customerList'
      callstandard="graphql"
      height="60vh"
    />
  );
});

// Type and required properties
Customers.propTypes = {};
// Default properties
Customers.defaultProps = {};

export default Customers;
