import React from "react";
import PropTypes from "prop-types";
import { Core, Styles } from "@ebs/styleguide";
// CORE COMPONENTS AND MOLECULES TO USE
import ReportManagement from "components/molecule/Reports";
const { makeStyles } = Styles;
const { Tab, Tabs, AppBar } = Core;

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && (
        <div style={{ maxWidth: "100%", overflow: "auto" }}>{children}</div>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `tab-${index}`,
    "aria-controls": `tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

const SettingOrganism = React.forwardRef((props, ref) => {
  // Properties of the organism
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="setting management"
        >
          <Tab label="PrintOut Manager" {...a11yProps(0)} />
          <Tab label="Sequence Rules" {...a11yProps(1)} />
          <Tab label="Email Templates" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <ReportManagement />
      </TabPanel>
      <TabPanel value={value} index={1}></TabPanel>
      <TabPanel value={value} index={2}></TabPanel>
      <TabPanel value={value} index={3}></TabPanel>
    </div>
  );
});
// Type and required properties
SettingOrganism.propTypes = {};
// Default properties
SettingOrganism.defaultProps = {};

export default SettingOrganism;
