import React, { memo } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import { transform } from "node-json-transform";
import { FIND_USER_LIST } from "utils/apollo/gql/user";
import Client from "utils/apollo";
import { FormattedMessage } from "react-intl";
const { IconButton, Typography } = Core;

//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Users = React.forwardRef(({}, ref) => {
  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="User" />
        </Typography>
      ),
      accessor: "userName",
      csvHeader: "User",
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Full Name" />
        </Typography>
      ),
      Cell: ({row}) => (
        <Typography variant="body1">
          {`${row.values["contact.person.givenName"]} ${row.values["contact.person.familyName"]}`}
        </Typography>
      ),
      accessor: "contact.person.givenName",
      csvHeader: "Full Name",
      width: 400,
    },
    {
      Header: "familyName",
      accessor: "contact.person.familyName",
      hidden: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Title" />
        </Typography>
      ),
      accessor: "contact.person.jobTitle",
      csvHeader: "Title",
      width: 400,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Knows About" />
        </Typography>
      ),
      accessor: "contact.person.knowsAbout",
      csvHeader: "Knows About",
      width: 200,
    },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(selectedRows);
          refresh();
        }}
        color="inherit"
      >
        <AddIcon />
      </IconButton>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(JSON.stringify(rowData));
          refresh();
        }}
        color="inherit"
      >
        <AddIcon />
      </IconButton>
    );
  };

  async function fetch({ page, sort, filters }) {
    return new Promise((resolve, reject) => {
      Client.query({
        query: FIND_USER_LIST,
        variables: {
          page: page,
          sort: sort,
          filters: filters,
          disjunctionFilters: filters.length > 1 ? true : false,
        },
      })
        .then(({ data }) => {
          const map = {
            item: {
              id: "id",
              name: "userName",
              givenName: "person.givenName",
              familyName: "person.familyName",
              jobTitle: "person.jobTitle",
              knowsAbout: "person.knowsAbout",
              language: "person.language.name",
            },
            each: function (item) {
              item.fullName = `${item.familyName.toUpperCase()}, ${
                item.givenName
              }`;
              return item;
            },
          };
          //transform data
          const result = transform(data.findUserList.content, map).sort(
            function (a, b) {
              return a.id - b.id;
            }
          );
          resolve({
            pages: data.findUserList.totalPages,
            data: result,
            elements: data.findUserList.totalElements,
          });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      toolbar={true}
      columns={columns}
      entity="User"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Registers" />
        </Typography>
      }
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      csvfilename='userList'
      callstandard="graphql"
      select="multi"
      height="40vh"
    />
  );
});

// Type and required properties
Users.propTypes = {};
// Default properties
Users.defaultProps = {};

export default memo(Users);
