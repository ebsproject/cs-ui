import React, { memo } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
const { Checkbox, Typography } = Core;

//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Organizations = React.forwardRef(({}, ref) => {
  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organization" />
        </Typography>
      ),
      accessor: "name",
      csvHeader: "Organization",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Legal Name" />
        </Typography>
      ),
      accessor: "legalName",
      csvHeader: "Legal Name",
      filter: true,
      width: 250,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Slogan" />
        </Typography>
      ),
      accessor: "slogan",
      csvHeader: "Slogan",
      filter: true,
      width: 400,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="WebPage" />
        </Typography>
      ),
      accessor: "webPage",
      csvHeader: "WebPage",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Logo" />
        </Typography>
      ),
      accessor: "logo",
      csvHeader: "Logo",
      filter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Active" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Checkbox disabled checked={value} />;
      },
      accessor: "active",
      csvHeader: "Active",
      disableFilters: true,
      disableResizing: true,
      disableGlobalFilter: true,
      width: 100,
    },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(selectedRows);
          refresh();
        }}
        color="inherit"
      >
        <AddIcon />
      </IconButton>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(JSON.stringify(rowData));
          refresh();
        }}
        color="inherit"
      >
        <AddIcon />
      </IconButton>
    );
  };

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      toolbar={true}
      columns={columns}
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      entity="Organization"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="List" />
        </Typography>
      }
      csvfilename='organizationList'
      callstandard="graphql"
      select="multi"
      height="60vh"
    />
  );
});

// Type and required properties
Organizations.propTypes = {};
// Default properties
Organizations.defaultProps = {};

export default memo(Organizations);
