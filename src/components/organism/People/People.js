import React, { memo } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
import Client from "utils/apollo";
import { FormattedMessage } from "react-intl";
import { EbsGrid } from "@ebs/components";
import Person from "components/molecule/Person";
import DeleteContact from "components/molecule/DeleteContact";
const { Typography } = Core;
import { FIND_CONTACT_LIST } from "utils/apollo/gql/crm";

//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const People = React.forwardRef(({}, ref) => {
  // Columns
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Family Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "person.familyName",
      csvHeader: "Family Name",
      width: 500,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Given Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "person.givenName",
      csvHeader: "Category",
      width: 500,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Additional name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "person.additionalName",
      csvHeader: "Additional Name",
      width: 500,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Job Title" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "person.jobTitle",
      csvHeader: "Job Title",
      width: 500,
    },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <div>
        <Person refresh={refresh} />
      </div>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <div className="flex flex-row flex-nowrap gap-2">
        <Person type="put" contact={rowData} refresh={refresh} />
        <DeleteContact contactId={rowData.id} refresh={refresh} />
      </div>
    );
  };

  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        Client.query({
          query: FIND_CONTACT_LIST,
          variables: {
            page: page,
            sort: sort,
            filters: [
              ...filters,
              { col: "category", mod: "EQ", val: "Person" },
            ],
            disjunctionFilters: filters.length > 1 ? true : false,
          },
          fetchPolicy: "no-cache",
        }).then(({ data, errors }) => {
          if (errors) {
            reject(errors);
          } else {
            resolve({
              pages: data.findContactList.totalPages,
              elements: data.findContactList.totalElements,
              data: data.findContactList.content,
            });
          }
        });
      } catch (e) {
        console.log(e);
      } finally {
        console.log("finally people");
      }
    });
  };

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      toolbar={true}
      data-testid={"PersonsGridTestId"}
      columns={columns}
      toolbaractions={toolbarActions}
      rowactions={rowActions}
      csvfilename="peopleList"
      fetch={fetch}
      height="85vh"
      select="multi"
    />
  );
});

// Type and required properties
People.propTypes = {};
// Default properties
People.defaultProps = {};

export default memo(People);
