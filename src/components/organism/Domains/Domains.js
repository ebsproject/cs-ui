import React, { memo } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import ProductList from "components/molecule/ProductList";
import NewDomainsMenuButton from "components/molecule/NewDomainsMenuButton";
import { FormattedMessage } from "react-intl";
const { Box, Typography } = Core;

//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Domains = React.forwardRef(({}, ref) => {
  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Name" />
        </Typography>
      ),
      accessor: "name",
      csvHeader: "Name",
      filter: true,
      width: 450,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Description" />
        </Typography>
      ),
      accessor: "info",
      csvHeader: "Description",
      filter: true,
      width: 550,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Icon" />
        </Typography>
      ),
      accessor: "icon",
      csvHeader: "Icon",
      filter: true,
      width: 450,
    },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <Box>
        <Box>
          <NewDomainsMenuButton
            rowSelected={(selectedRows.length > 0 && selectedRows[0]) || null}
            refresh={refresh}
          />
        </Box>
      </Box>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(JSON.stringify(rowData));
          refresh();
        }}
        color="inherit"
      >
        <AddIcon />
      </IconButton>
    );
  };

  const ProductDetail = (rowData) => {
    return <ProductList rowData={rowData} />;
  };

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      toolbar={true}
      columns={columns}
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      entity="Domain"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="List" />
        </Typography>
      }
      csvfilename='domainList'
      // toolbaractions={toolbarActions}
      detailcomponent={ProductDetail}
      callstandard="graphql"
      select="single"
      height="60vh"
    />
  );
});

// Type and required properties
Domains.propTypes = {};
// Default properties
Domains.defaultProps = {};

export default memo(Domains);
