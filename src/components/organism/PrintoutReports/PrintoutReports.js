import React, { memo } from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { EbsGrid } from "@ebs/components";
import { Core } from "@ebs/styleguide";
import NewReportsMenuButton from "components/molecule/NewReportsMenu";
import DeleteReportButton from "components/molecule/DeleteReportButton";
import ReportDesignerButton from "components/molecule/ReportDesignerButton";
import ReportsDetail from "components/molecule/ReportsDetail";
import ModifyReportButton from "components/molecule/ModifyReportButton";
const { Box, Typography } = Core;

//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const PrintoutReports = React.forwardRef(({}, ref) => {
  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true, filter: false },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Name" />
        </Typography>
      ),
      csvHeader: "Name",
      accessor: "name",
      width: 500,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Description" />
        </Typography>
      ),
      csvHeader: "Description",
      accessor: "description",
      filter: true,
      width: 500,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="ZPL" />
        </Typography>
      ),
      csvHeader: "ZPL",
      accessor: "zpl",
      filter: true,
      width: 550,
    },
    {
      Header: "programs.id",
      accessor: "programs.id",
      hidden: true,
      filter: false,
    },
    {
      Header: "programs.name",
      accessor: "programs.name",
      hidden: true,
      filter: false,
    },
    {
      Header: "products.id",
      accessor: "products.id",
      hidden: true,
      filter: false,
    },
    {
      Header: "products.name",
      accessor: "products.name",
      hidden: true,
      filter: false,
    },
    {
      Header: "products.domain.name",
      accessor: "products.domain.name",
      hidden: true,
      filter: false,
    },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <Box display="flex" flexDirection="row">
        <Box>
          <NewReportsMenuButton refresh={refresh} />
        </Box>
        <Box>
          <DeleteReportButton selectedRows={selectedRows} refresh={refresh} />
        </Box>
      </Box>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <Box display="flex" flexDirection="row">
        <Box>
          <ReportDesignerButton rowData={rowData} refresh={refresh} />
        </Box>
        <Box>
          <ModifyReportButton rowData={rowData} refresh={refresh} />
        </Box>
      </Box>
    );
  };

  const ReportDetail = (rowData) => {
    return (
      <ReportsDetail
        productsData={rowData.products}
        programsData={rowData.programs}
      />
    );
  };

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      toolbar={true}
      toolbaractions={toolbarActions}
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Reports" />
        </Typography>
      }
      columns={columns}
      entity="PrintoutTemplate"
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      detailcomponent={ReportDetail}
      rowactions={rowActions}
      csvfilename='reportList'
      callstandard="graphql"
      height="60vh"
      select="multi"
    />
  );
});

// Type and required properties
PrintoutReports.propTypes = {};
// Default properties
PrintoutReports.defaultProps = {};

export default memo(PrintoutReports);
