import React, { memo } from "react";
import PropTypes from "prop-types";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
import ModifyTenant from "components/molecule/ModifyTenantButton";
const { Box, Typography } = Core;

//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Tenants = React.forwardRef(({}, ref) => {
  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Registration ID" />
        </Typography>
      ),
      csvHeader: "Registration ID",
      accessor: "name",
      width: 250,
    },
    {
      Header: "OrganizationId",
      accessor: "organization.id",
      disableGlobalFilter: true,
      hidden: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organization" />
        </Typography>
      ),
      csvHeader: "Organization",
      accessor: "organization.name",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Legal Name" />
        </Typography>
      ),
      csvHeader: "Legal Name",
      accessor: "organization.legalName",
      disableGlobalFilter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Logo" />
        </Typography>
      ),
      csvHeader: "Logo",
      accessor: "organization.logo",
      disableGlobalFilter: true,
      width: 200,
    },
    {
      Header: "CustomerId",
      accessor: "customer.id",
      disableGlobalFilter: true,
      hidden: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Customer" />
        </Typography>
      ),
      csvHeader: "Customer",
      accessor: "customer.name",
      disableGlobalFilter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Customer Logo" />
        </Typography>
      ),
      csvHeader: "Customer Logo",
      accessor: "customer.logo",
      disableGlobalFilter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Expiration" />
        </Typography>
      ),
      csvHeader: "Expiration",
      accessor: "expiration",
      disableGlobalFilter: true,
      width: 200,
    },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(selectedRows);
          refresh();
        }}
        color="inherit"
      >
        <AddIcon />
      </IconButton>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <Box>
        <Box>
          <ModifyTenant rowData={rowData} refresh={refresh} />
        </Box>
      </Box>
    );
  };

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      toolbar={true}
      columns={columns}
      entity="Tenant"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Registers" />
        </Typography>
      }
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      select="multi"
      csvfilename='tenantList'
      rowactions={rowActions}
      callstandard="graphql"
      height="65vh"
    />
  );
});

// Type and required properties
Tenants.propTypes = {};
// Default properties
Tenants.defaultProps = {};

export default memo(Tenants);
