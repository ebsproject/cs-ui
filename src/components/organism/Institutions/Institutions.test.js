  import Institutions from './Institutions';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Institutions is in the DOM', () => {
  render(<Institutions></Institutions>)
  expect(screen.getByTestId('InstitutionsTestId')).toBeInTheDocument();
})
