import React from "react";
import PropTypes from "prop-types";
import Client from "utils/apollo";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { EbsGrid } from "@ebs/components";
import { Core, Icons } from "@ebs/styleguide";
// CORE COMPONENTS
import DeleteContact from "components/molecule/DeleteContact";
import Institution from "components/molecule/Institution";
const { Typography } = Core;
import { FIND_CONTACT_LIST } from "utils/apollo/gql/crm";

//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Institutions = React.forwardRef(({}, ref) => {
  // Columns
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Common Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.commonName",
      csvHeader: "Common Name",
      width: 500,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Legal Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.legalName",
      csvHeader: "Category",
      width: 500,
    },
    {
      Header: "principalContactId",
      accessor: "institution.principalContact.person.id",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Contact Family Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.principalContact.person.familyName",
      csvHeader: "Contact Family Name",
      width: 500,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Contact Given Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.principalContact.person.givenName",
      csvHeader: "Contact Given Name",
      width: 500,
    },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <div>
        <Institution refresh={refresh} />
      </div>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <div className="flex flex-row flex-nowrap gap-2">
        {/* <Institution type="put" contact={rowData} refresh={refresh} /> */}
        <DeleteContact contactId={rowData.id} refresh={refresh} />
      </div>
    );
  };

  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        Client.query({
          query: FIND_CONTACT_LIST,
          variables: {
            page: page,
            sort: sort,
            filters: [
              ...filters,
              { col: "category", mod: "EQ", val: "Institution" },
            ],
            disjunctionFilters: filters.length > 0 ? true : false,
          },
          fetchPolicy: "no-cache",
        }).then(({ data, errors }) => {
          if (errors) {
            reject(errors);
          } else {
            resolve({
              pages: data.findContactList.totalPages,
              elements: data.findContactList.totalElements,
              data: data.findContactList.content,
            });
          }
        });
      } catch (e) {
        console.log(e);
      } finally {
        console.log("finally people");
      }
    });
  };

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      toolbar={true}
      data-testid={"InstitutionsGridTestId"}
      columns={columns}
      toolbaractions={toolbarActions}
      rowactions={rowActions}
      csvfilename="institutionList"
      fetch={fetch}
      height="85vh"
      select="multi"
    />
  );
});

// Type and required properties
Institutions.propTypes = {};
// Default properties
Institutions.defaultProps = {};

export default Institutions;
