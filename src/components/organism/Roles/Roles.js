import React, { memo } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
const { IconButton, Typography } = Core;

//MAIN FUNCTION
/*
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Roles = React.forwardRef(({}, ref) => {
  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Role" />
        </Typography>
      ),
      csvHeader: "Role",
      accessor: "name",
      filter: true,
      width: 800,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="AD Security Group" />
        </Typography>
      ),
      csvHeader: "AD Security Group",
      accessor: "securityGroup",
      filter: true,
      width: 800,
    },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(selectedRows);
          refresh();
        }}
        color="inherit"
      >
        <AddIcon />
      </IconButton>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(JSON.stringify(rowData));
          refresh();
        }}
        color="inherit"
      >
        <AddIcon />
      </IconButton>
    );
  };

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      toolbar={false}
      columns={columns}
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      entity="Role"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Roles" />
        </Typography>
      }
      csvfilename='roleList'
      callstandard="graphql"
      select="multi"
      height="65vh"
    />
  );
});

// Type and required properties
Roles.propTypes = {};
// Default properties
Roles.defaultProps = {};

export default memo(Roles);
