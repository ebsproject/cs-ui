import React from "react";
import EbsSplash from "assets/images/logos/EBS_V-W.svg";
import { Core } from "@ebs/styleguide";
const { CircularProgress } = Core;

const EBSSplashScreenAtom = React.forwardRef((props, ref) => {
  return (
    <div className="flex flex-col h-screen justify-center items-center bg-ebs-splash">
      <div className="">
        <img className={"m-16"} width="128" src={EbsSplash} alt="logo" />
      </div>
      <div className="">
        <CircularProgress />
      </div>
    </div>
  );
});

export default EBSSplashScreenAtom;
