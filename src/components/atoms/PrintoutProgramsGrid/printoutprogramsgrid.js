import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { Typography } = Core;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PrintoutProgramsGridAtom = React.forwardRef(({ data }, ref) => {
  const columns = [
    { Header: "Id", accessor: "id", hidden: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Name" />
        </Typography>
      ),
      accessor: "name",
      width: 1200,
    },
  ];

  async function fetch({ page, sort, filters }) {
    return new Promise((resolve, reject) => {
      resolve({ data: data, pages: 1, elements: data.length });
    });
  }

  return (
    /* 
     @prop data-testid: Id to use inside printoutprogramsgrid.test.js file.
     */
    <div data-testid={"PrintoutProgramsGridTestId"} ref={ref}>
      <EbsGrid toolbar={false} columns={columns} fetch={fetch} height="20vh" />
    </div>
  );
});
// Type and required properties
PrintoutProgramsGridAtom.propTypes = {
  data: PropTypes.array.isRequired,
};
// Default properties
PrintoutProgramsGridAtom.defaultProps = {};

export default PrintoutProgramsGridAtom;
