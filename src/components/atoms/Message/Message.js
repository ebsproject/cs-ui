import { Core } from "@ebs/styleguide";
import { memo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { hideMessage } from "store/ducks/message";
const { Icon, IconButton, Snackbar, SnackbarContent, Typography } = Core;

const variantIcon = {
  success: "check_circle",
  warning: "warning",
  error: "error_outline",
  info: "info",
};

function FuseMessage(props) {
  const dispatch = useDispatch();
  const state = useSelector((store) => store.message.state);
  const options = useSelector((store) => store.message.options);
  const variants = {
    success: "text-white bg-green-600",
    error: "text-white bg-red-700",
    info: "text-white bg-blue-600",
    warning: "text-white bg-yellow-700",
  };

  return (
    <Snackbar
      {...options}
      open={state}
      onClose={() => dispatch(hideMessage())}
      ContentProps={{
        variant: "body2",
        headlineMapping: {
          body1: "div",
          body2: "div",
        },
      }}
    >
      <SnackbarContent
        className={variants[options.variant]}
        message={
          <div className="flex items-center">
            {variantIcon[options.variant] && (
              <Icon color="inherit">{variantIcon[options.variant]}</Icon>
            )}
            <Typography className="mx-8">{options.message}</Typography>
          </div>
        }
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            onClick={() => dispatch(hideMessage())}
          >
            <Icon>close</Icon>
          </IconButton>,
        ]}
      />
    </Snackbar>
  );
}

export default memo(FuseMessage);
