import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { Typography } = Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PrintoutProductsGridAtom = React.forwardRef(({ data }, ref) => {
  const columns = [
    { Header: "Id", accessor: "id", hidden: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Name" />{" "}
        </Typography>
      ),
      accessor: "name",
      width: 600,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Domain" />{" "}
        </Typography>
      ),
      accessor: "domain",
      width: 600,
    },
  ];

  async function fetch({ page, sort, filters }) {
    return new Promise((resolve, reject) => {
      let newData = [];
      data.map((product) => {
        newData.push({
          id: product.id,
          name: product.name,
          domain: product.domain.name,
        });
      });
      resolve({ data: newData, pages: 1, elements: data.length });
    });
  }

  return (
    /* 
     @prop data-testid: Id to use inside printoutproductsgrid.test.js file.
     */
    <div data-testid={"PrintoutProductsGridTestId"} ref={ref}>
      <EbsGrid toolbar={false} columns={columns} fetch={fetch} height="20vh" />
    </div>
  );
});
// Type and required properties
PrintoutProductsGridAtom.propTypes = {
  data: PropTypes.array.isRequired,
};
// Default properties
PrintoutProductsGridAtom.defaultProps = {};

export default PrintoutProductsGridAtom;
