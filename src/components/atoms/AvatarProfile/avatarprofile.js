import React, { Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Core, Styles } from "@ebs/styleguide";
import { useHistory } from "react-router-dom";
import { signOut } from "store/ducks/auth";
import { FormattedMessage } from "react-intl";
const { makeStyles } = Styles;
const {
  Popover,
  Typography,
  Icon,
  ListItemText,
  Avatar,
  Chip,
  MenuItem,
  Link,
  ListItemIcon,
  Divider,
} = Core;

const useStyles = makeStyles((theme) => ({
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
}));
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AvatarProfileAtom = React.forwardRef((props, ref) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [placement, setPlacement] = React.useState();
  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();
  const { userInfo } = useSelector(({ user }) => user);

  const handleClick = (newPlacement) => (event) => {
    setAnchorEl(event.currentTarget);
    setOpen((prev) => placement !== newPlacement || !prev);
    setPlacement(newPlacement);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleLogout = () => {
    dispatch(signOut());
    history.push("/login");
  };

  if (userInfo) {
    const { person: personInfo } = userInfo.contact;
    return (
      /* 
     @prop data-testid: Id to use inside avatarprofile.test.js file.
     */
      <React.Fragment>
        {userInfo.userName ? (
          <Chip
            avatar={
              <Avatar className="" alt="user photo" src={userInfo.photo} />
            }
            label={
              <React.Fragment>
                <Typography
                  className={classes.title}
                  variant="subtitle1"
                  noWrap
                >
                  {`${personInfo?.familyName}, ${personInfo?.givenName}`}
                </Typography>
                <Divider />
                <Typography
                  className={classes.title}
                  variant="subtitle2"
                  noWrap
                >
                  {personInfo?.jobTitle}
                </Typography>
              </React.Fragment>
            }
            onClick={handleClick("bottom-end")}
            variant="default"
            color="primary"
            clickable
          />
        ) : (
          <Chip size="small" />
        )}
        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
        >
          <MenuItem onClick={handleLogout}>
            <ListItemIcon>
              <Icon>exit_to_app</Icon>
            </ListItemIcon>
            <ListItemText
              primary={
                <FormattedMessage
                  id="tnt.nav.profile.lbl.logout"
                  defaultMessage="Logout"
                />
              }
            />
          </MenuItem>
        </Popover>
      </React.Fragment>
    );
  } else {
    return <Fragment />;
  }
});
// Type and required properties
AvatarProfileAtom.propTypes = {};
// Default properties
AvatarProfileAtom.defaultProps = {};

export default AvatarProfileAtom;
