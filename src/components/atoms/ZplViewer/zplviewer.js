import React from "react";
import PropTypes from "prop-types";
import axios from "axios";
// CORE COMPONENTS

const client = axios.create({
  baseURL: "http://localhost:8083", //process.env.REACT_APP_CSAPI_URI_GRAPHQL
  headers: {
    Accept: "image/png",
    "Content-Type": "application/json",
  },
});

const client2 = axios.create({
  baseURL: "http://localhost:8080",
  headers: {
    "Content-Type": "application/json",
  },
});

// function hexToBase64(str) {
//   return btoa(String.fromCharCode.apply(null, str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" ")));
// }

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ZplViewerAtom = React.forwardRef((props, ref) => {
  const [image, setImage] = React.useState(null);
  const imgRef = React.createRef();
  React.useLayoutEffect(() => {
    client2
      .post("graphql", {
        query: `{findReport(id:1){ name description zplCode dataSource }}`,
      })
      .then(({ data }) => {
        const infoReport = data.data.findReport;
        // console.log(infoReport.zplCode);
        client
          .post("ZPLPrinter", {
            zplCommands: infoReport.zplCode,
          })
          .then(({ data }) => {
            const blob = new Blob([data], {
              type: "image/png",
            });
            const url = URL.createObjectURL(blob);
            setImage(true);
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  }, [image]);

  return (
    image && (
      /* 
     @prop data-testid: Id to use inside zplviewer.test.js file.
     */
      <div data-testid={"ZplViewerTestId"} ref={ref}>
        <img ref={imgRef} id="img" />
      </div>
    )
  );
});
// Type and required properties
ZplViewerAtom.propTypes = {};
// Default properties
ZplViewerAtom.defaultProps = {};

export default ZplViewerAtom;
