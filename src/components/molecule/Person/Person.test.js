import Person from "./Person";
import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";

afterEach(cleanup);

test("Person is in the DOM", async () => {
  const { getByTestId } = render(
    <Person
      type="put"
      refresh={() => {}}
      contact={{
        id: 1,
        person: { familyName: "test", givenName: "test" },
      }}
    />,
    { wrapper: Wrapper }
  );
  expect(getByTestId("PersonTestId")).toBeInTheDocument();
});

test("User can save a new contact", async () => {
  const { getByTestId, getByText, queryByRole } = render(
    <Person
      type="put"
      refresh={() => {}}
      contact={{
        id: 1,
        person: { familyName: "test", givenName: "test" },
      }}
    />,
    { wrapper: Wrapper }
  );
  fireEvent.click(getByTestId("PutButton"));
  expect(getByText(/modify person/i)).toBeInTheDocument();
  userEvent.type(getByTestId("familyName"), "test");
  expect(getByTestId("familyName")).toHaveValue("test");
  userEvent.type(getByTestId("givenName"), "test");
  expect(getByTestId("givenName")).toHaveValue("test");
  userEvent.type(getByTestId("additionalName"), "test");
  expect(getByTestId("additionalName")).toHaveValue("test");
  userEvent.type(getByTestId("officialEmail"), "test@mail.com");
  expect(getByTestId("officialEmail")).toHaveValue("test@mail.com");
  userEvent.type(getByTestId("phoneDefault"), "(123)-781-276");
  // expect(getByTestId('contactType')).toHaveValue('test');
  // userEvent.type(getByTestId("familyName"), "test");
  // expect(getByTestId('familyName')).toHaveValue('test');
  // const contactType = getByTestId("contactType");
  // fireEvent.mouseDown(contactType);
  // const ListBox = globalGetByRole(contactType, "textbox");
  // expect(ListBox).toBeDefined();
  // fireEvent.click(globalGetByText(ListBox, "mock"));
  // expect(queryByRole("textbox")).toBeNull();
  // await userEvent.selectOptions(getByTestId('contactType'),{ name: "mock", category: "Person" })
  // screen.debug();
});
