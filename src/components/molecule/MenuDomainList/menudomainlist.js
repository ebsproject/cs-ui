import React, { Fragment } from "react";
import { Core, Styles } from "@ebs/styleguide";
import { useSelector, useDispatch, useStore } from "react-redux";
import { useHistory } from "react-router-dom";
// CORE COMPONENTS AND ATOMS TO USE
import { showMessage } from "store/ducks/message";
import { setTenantContext } from "store/ducks/tenant";
//MAIN FUNCTION
const { makeStyles } = Styles;
const { ListItem, ListItemText, Icon, ListItemIcon } = Core;

// Import all Icons
const iconList = require.context("assets/images/domains", true, /\.svg$/);
const icons = iconList.keys().reduce((images, path) => {
  images[path] = iconList(path);
  return images;
}, {});

const useStyles = makeStyles((theme) => ({
  imageIcon: {
    display: "flex",
    height: "inherit",
    width: "inherit",
  },
  imageIcon: {
    height: "100%",
  },
}));

const MenuDomainListMolecule = React.forwardRef((props, ref) => {
  //redux
  const dispatch = useDispatch();
  const { getState } = useStore();
  const classes = useStyles();
  const history = useHistory();
  const { instanceInfo, tenantContext } = useSelector(({ tenant }) => tenant);

  //events
  const handleClickDomain = (item) => {
    if (item.mfe) {
      setTenantContext({ domainId: Number(item.domain.id) })(
        dispatch,
        getState
      );
      history.push(`/${item.domain.prefix}`);
    } else {
      item.context
        ? window.open(item.context, "__blank")
        : dispatch(
            showMessage({
              message: "The component does not have URL",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
    }
  };

  return (
    <Fragment>
      {instanceInfo?.domaininstances
        .slice()
        .sort(function (a, b) {
          return a.id - b.id;
        })
        .filter(function (item) {
          return item.domain.id !== tenantContext.domainId;
        })
        .map((r) => (
          <ListItem button key={r.id} onClick={(event) => handleClickDomain(r)}>
            <ListItemIcon>
              <Icon
                classes={{ root: classes.iconRoot }}
                color="inherit"
                aria-label="open drawer"
                edge="start"
              >
                <img
                  className={classes.imageIcon}
                  src={icons[`./${r.domain.icon}`]}
                />
              </Icon>
            </ListItemIcon>
            <ListItemText primary={r.domain.name}></ListItemText>
          </ListItem>
        ))}
    </Fragment>
  );
});
// Type and required properties
MenuDomainListMolecule.propTypes = {};
// Default properties
MenuDomainListMolecule.defaultProps = {};

export default MenuDomainListMolecule;
