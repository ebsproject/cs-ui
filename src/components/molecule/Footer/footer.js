import React from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { Core } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { useSelector } from "react-redux";
const { Typography } = Core;

const FooterMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { tenantInfo, tenantContext } = useSelector(({ tenant }) => tenant);

  return (
    <div data-testid={"FooterTestId"} className="flex flex-row flex-nowrap">
      <Typography variant="subtitle1" color="textPrimary">
        Copyright@2021 Suscription for Customer: {tenantInfo?.customer.name},
        Organization: {tenantInfo?.organization.name}, Session: {tenantInfo?.name}
        , Instance:{" "}
        {
          tenantInfo?.instances.find((f) => f.id == tenantContext?.instanceId)
            .name
        }
      </Typography>
    </div>
  );
});

// Type and required properties
FooterMolecule.propTypes = {};
// Default properties
FooterMolecule.defaultProps = {};

export default FooterMolecule;
