import React, {
  useState,
  useEffect,
  memo,
  Fragment,
  useCallback,
  useRef,
} from "react";
import PropTypes from "prop-types";
import { EbsDialog, Core, Icons } from "@ebs/styleguide";
import { useSelector, useDispatch, useStore } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import PhoneInput from "components/atoms/PhoneInput";
import { useForm, Controller } from "react-hook-form";
import { Autocomplete } from "@material-ui/lab";
import {
  findContactTypeList,
  findCountryList,
  createContact,
  modifyContact,
} from "store/ducks/crm";
import { useQuery } from "@apollo/client";
import { FIND_CONTACT } from "utils/apollo/gql/crm";
import { FIND_CONTACT_LIST } from "utils/apollo/gql/crm";
import Client from "utils/apollo";
const { Add, Close, PostAdd, Edit } = Icons;
const {
  Box,
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  TextField,
  Tooltip,
  Typography,
  FormControl,
  InputLabel,
  Input,
  Checkbox,
  FormControlLabel,
  CircularProgress,
} = Core;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const InstitutionMolecule = React.forwardRef(
  ({ refresh, type, contact }, ref) => {
    console.log(contact)
    const [principalContact, setPrincipalContact] = useState("");
    const [open, setOpen] = useState(false);
    const [phones, setPhones] = useState([]);
    const [loading, setLoading] = useState(false);
    const [contactList, setContactList] = useState([]);
    const [address, setAddress] = useState([]);
    const [addressToDelete, setAddressToDelete] = useState([]);
    const [phonesToDelete, setPhonesToDelete] = useState([]);
    const [defaultValues, setDefaultValues] = useState({});
    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
    } = useForm();
    const dispatch = useDispatch();
    const { getState } = useStore();
    const { contactTypeList, countryList, success } = useSelector(
      ({ crm }) => crm
    );

    useEffect(() => {
      getContactList(principalContact);
    }, [getContactList, principalContact]);

    /*
     * Hook to handle Async functions on principalContact Input
     */
    function useBouncer(fn, delay) {
      const ref = useRef({});
      ref.current.fn = fn;
      const bouncer = useCallback(
        (...args) => {
          // * Clear the old Timeout if one exist
          if (ref.current.timeout) {
            clearTimeout(ref.current.timeout);
          }
          ref.current.timeout = setTimeout(() => {
            ref.current.fn(...args);
            ref.current.timeout = undefined;
          }, delay);
        },
        [delay]
      );
      return bouncer;
    }

    const getContactList = useBouncer(async (value) => {
      setLoading(true)
      Client.query({
        query: FIND_CONTACT_LIST,
        variables: {
          page: { number: 1, size: 10 },
          filters: [
            { col: "person.familyName", mod: "LK", val: value },
            { col: "person.givenName", mod: "LK", val: value },
          ],
          disjunctionFilters: true,
        },
        fetchPolicy: "no-cache",
      }).then(({ data, errors }) => {
        setLoading(false)
        if (errors) {
          console.log(errors);
        } else {
          setContactList(data.findContactList.content);
        }
      });
    }, 300);

    const { data: contactData } =
      type === "put" &&
      useQuery(FIND_CONTACT, {
        variables: { id: contact.id },
        fetchPolicy: "no-cache",
      });

    const List = contactTypeList?.filter(
      (contactType) => contactType.category === "Institution"
    );

    useEffect(() => {
      if (type === "put" && contactData) {
        const { website, phones } = contactInfos(
          contactData.findContact.contactInfos
        );
        let addresses = [...contactData.findContact.addresses];
        setDefaultValues({
          principalContact: contact?.institution?.principalContact,
          commonName: contact?.institution?.commonName,
          legalName: contact?.institution?.legalName,
          website: website,
          phoneDefault: phones?.[0],
          contactType: contactData?.findContact?.contactTypes,
          addressDefault: addresses?.[0],
        });
        phones?.shift();
        setPhones(phones);
        addresses.shift();
        setAddress(addresses);
        setPhonesToDelete([]);
        setAddressToDelete([]);
      }
    }, [contactData, open]);

    useEffect(() => {
      if (success) {
        handleClose();
        reset();
        refresh();
      }
    }, [success]);

    useEffect(() => {
      !contactTypeList && findContactTypeList()(dispatch, getState);
      !countryList && findCountryList()(dispatch, getState);
    }, [contactTypeList, countryList]);

    function contactInfos(contactInfosList) {
      let contactInfos = new Object();
      let phones = new Array();
      contactInfosList.map(({ contactInfoType, value, id }, key) => {
        switch (contactInfoType.id) {
          case 1: // ? Phone number
            phones.push({ id, value });
            break;
          case 3: // ? Web page
            Object.assign(contactInfos, { website: { id, value } });
            break;
          default:
            break;
        }
      });
      Object.assign(contactInfos, { phones: phones });
      return contactInfos;
    }

    function contactTypeIds(contactTypeList) {
      let contactTypeIds = new Array();
      contactTypeList.map((contact, key) => {
        contactTypeIds.push(contact.id);
      });
      return contactTypeIds;
    }

    const onSubmit = (data) => {
      let addresses = new Array();
      let contacts = new Array();
      // * Adding default Address
      addresses.push({
        id: Number(defaultValues?.addressDefault?.id) || 0,
        location: data["locality-default"],
        region: data["region-default"],
        zipCode: data["postalCode-default"],
        streetAddress: data["street-default"],
        default: data["default-default"] || false,
        countryId:
          (data["country-default"] != undefined &&
            data["country-default"].id) ||
          Number(defaultValues?.addressDefault?.country?.id),
      });
      // ? There are more than one Address
      address?.map((address, index) =>
        addresses.push({
          id: Number(address?.id) || 0,
          location: data[`locality-${index}`],
          region: data[`region-${index}`],
          zipCode: data[`postalCode-${index}`],
          streetAddress: data[`street-${index}`],
          default: data[`default-${index}`] || false,
          countryId: data[`country-${index}`].id,
        })
      );
      // * Adding website
      contacts.push({
        id: defaultValues?.website?.id || 0,
        value: data["website"],
        default: false,
        contactInfoTypeId: 3,
      });
      // * Adding default phone
      data["phone-default"] &&
        contacts.push({
          id: defaultValues?.phoneDefault?.id || 0,
          value: data["phone-default"],
          default: false,
          contactInfoTypeId: 1,
        });
      // ? There are more than one phone
      phones?.map((phone, index) =>
        contacts.push({
          id: phone?.id || 0,
          value: data[`phone-${index + 1}`],
          default: false,
          contactInfoTypeId: 1,
        })
      );

      (type === "post" &&
        createContact({
          id: 0,
          category: "Institution",
          contactTypeIds: data.contactType
            ? contactTypeIds(data?.contactType)
            : contactTypeIds(defaultValues?.contactType),
          addresses: addresses,
          contactInfos: contacts,
          institution: {
            commonName: data.commonName,
            legalName: data.legalName,
            principalContactId: data.principalContact.id,
          },
        })(dispatch, getState)) ||
        modifyContact(
          {
            id: contact.id,
            category: "Institution",
            contactTypeIds: data.contactType
              ? contactTypeIds(data?.contactType)
              : contactTypeIds(defaultValues?.contactType),
            addresses: addresses,
            contactInfos: contacts,
            institution: {
              commonName: data.commonName,
              legalName: data.legalName,
              principalContactId: data.principalContact.id,
            },
          },
          { address: addressToDelete, phones: phonesToDelete }
        )(dispatch, getState);
    };

    const handleClickOpen = () => {
      reset();
      setOpen(true);
    };

    function handleClose() {
      setOpen(false);
    }

    const handleAddPhone = () => {
      let newPhones = phones;
      newPhones.push(phones.length + 1);
      setPhones([...newPhones]);
    };

    const handleRemovePhone = (phone, index) => {
      let newPhones = phones;
      let newPhonesToDelete = phonesToDelete;
      phone.id && newPhonesToDelete.push(phone);
      newPhones.splice(index, 1);
      setPhonesToDelete(newPhonesToDelete);
      setPhones([...newPhones]);
    };

    const handleAddAddress = () => {
      let newAddress = address;
      newAddress.push(address.length + 1);
      setAddress([...newAddress]);
    };

    const handleRemoveAddress = (addressObj, index) => {
      let newAddress = address;
      let newAddressToDelete = addressToDelete;
      addressObj.id && newAddressToDelete.push(addressObj);
      newAddress.splice(index, 1);
      setAddressToDelete(newAddressToDelete);
      setAddress([...newAddress]);
    };

    const onInputChange = (e, value) => {
      setPrincipalContact(value);
    };

    return (
      /* 
     @prop data-testid: Id to use inside Institution.test.js file.
     */
      <Box
        component="div"
        ref={ref}
        data-testid={"InstitutionTestId"}
        className="flex flex-row"
      >
        {(type === "post" && (
          <Button
            variant="contained"
            aria-label="add-institution"
            className="bg-green-600 hover:bg-green-700 font-bold"
            startIcon={<PostAdd className="fill-current text-white" />}
            onClick={handleClickOpen}
            data-testid={"PostButton"}
          >
            <Typography className="text-white">
              <FormattedMessage id="none" defaultMessage="New Institution" />
            </Typography>
          </Button>
        )) || (
          <Tooltip
            title={
              <FormattedMessage id="none" defaultMessage="Modify Institution" />
            }
          >
            <IconButton
              onClick={handleClickOpen}
              aria-label="modify-institution"
              color="primary"
              data-testid={"PutButton"}
            >
              <Edit />
            </IconButton>
          </Tooltip>
        )}
        <EbsDialog
          open={open}
          handleClose={handleClose}
          title={
            type === "put" ? (
              <FormattedMessage id="none" defaultMessage="Modify Institution" />
            ) : (
              <FormattedMessage id="none" defaultMessage="New Institution" />
            )
          }
        >
          <form onSubmit={handleSubmit(onSubmit)} data-testid="institutionForm">
            <DialogContent
              dividers
              className="grid grid-cols-1 sm:grid-cols-2 gap-2 md:gap-6"
            >
              <Typography className="font-ebs col-span-2 text-ebs-green-900 font-bold">
                <FormattedMessage
                  id="none"
                  defaultMessage="Basic Information"
                />
              </Typography>
              <Controller
                name="commonName"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    required
                    label={<FormattedMessage id="none" defaultMessage="Name" />}
                    data-testid={"commonName"}
                  />
                )}
                defaultValue={defaultValues?.commonName}
              />
              <Controller
                name="legalName"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    required
                    label={
                      <FormattedMessage id="none" defaultMessage="Legal Name" />
                    }
                    data-testid={"legalName"}
                  />
                )}
                defaultValue={defaultValues?.legalName}
              />
              <Controller
                name="website"
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    label={
                      <FormattedMessage id="none" defaultMessage="Web Site" />
                    }
                    data-testid={"webSite"}
                  />
                )}
                defaultValue={defaultValues?.website?.value}
              />

              <Controller
                name="contactType"
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    // {...field}
                    multiple
                    id="contactType"
                    data-testid={"contactType"}
                    options={List}
                    disableCloseOnSelect
                    defaultValue={defaultValues?.contactType}
                    onChange={(e, options) => setValue("contactType", options)}
                    getOptionLabel={(option) => option.name}
                    renderOption={(option, { selected }) => (
                      <Fragment>
                        <Checkbox checked={selected} />
                        {option.name}
                      </Fragment>
                    )}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={errors["contactType"] ? true : false}
                        helperText={
                          errors["contactType"] && (
                            <Typography className="text-red-600 text-ebs">
                              <FormattedMessage
                                id="none"
                                defaultMessage="Please select"
                              />
                            </Typography>
                          )
                        }
                        label={
                          <FormattedMessage
                            id="none"
                            defaultMessage="Contact Type"
                          />
                        }
                      />
                    )}
                  />
                )}
                defaultValue={defaultValues?.contactType}
                rules={{ required: true }}
              />
              <div>
                <div className="grid grid-cols-1 sm:gap-2 md:gap-4">
                  <div>
                    <div className="grid grid-cols-4">
                      <Controller
                        name="phone-default"
                        control={control}
                        render={({ field }) => (
                          <FormControl className="col-span-3" required>
                            <InputLabel htmlFor="phone-default">
                              <FormattedMessage
                                id="none"
                                defaultMessage="Phone"
                              />
                            </InputLabel>
                            <Input
                              {...field}
                              name="phone-default"
                              id="phone-default"
                              inputComponent={PhoneInput}
                              data-testid={"phoneDefault"}
                            />
                          </FormControl>
                        )}
                        defaultValue={defaultValues?.phoneDefault?.value}
                      />
                      <div>
                        <Tooltip
                          title={
                            <FormattedMessage
                              id="none"
                              defaultMessage="Add another phone"
                            />
                          }
                        >
                          <IconButton
                            className="fill-current text-blue-400"
                            onClick={handleAddPhone}
                          >
                            <Add />
                          </IconButton>
                        </Tooltip>
                      </div>
                    </div>
                  </div>
                  {phones?.map((phone, index) => (
                    <div key={index}>
                      <div className="grid grid-cols-4">
                        <Controller
                          name={`phone-${phone}`}
                          control={control}
                          render={({ field }) => (
                            <FormControl className="col-span-3">
                              <InputLabel htmlFor={`phone-${phone}`}>
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="Phone"
                                />
                              </InputLabel>
                              <Input
                                {...field}
                                name={`phone-${phone}`}
                                id={`phone-${phone}`}
                                inputComponent={PhoneInput}
                                data-testid={`phone${phone}`}
                              />
                            </FormControl>
                          )}
                          defaultValue={phone?.value}
                        />
                        <div>
                          <Tooltip
                            title={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Remove phone"
                              />
                            }
                          >
                            <IconButton
                              className="fill-current text-red-400"
                              onClick={() => handleRemovePhone(phone, index)}
                            >
                              <Close />
                            </IconButton>
                          </Tooltip>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              <Controller
                name="principalContact"
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    {...field}
                    id="principal-contact"
                    data-testid={"principalContact"}
                    options={contactList}
                    loading={loading}
                    onChange={(e, options) =>
                      setValue("principalContact", options)
                    }
                    defaultValue={defaultValues?.principalContact}
                    getOptionLabel={(option) =>
                      `${option.person.familyName}, ${option.person.givenName}`
                    }
                    onInputChange={onInputChange}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        error={errors["principalContact"] ? true : false}
                        helperText={
                          errors["principalContact"] && (
                            <Typography className="text-red-600 text-ebs">
                              <FormattedMessage
                                id="none"
                                defaultMessage="Please select"
                              />
                            </Typography>
                          )
                        }
                        label={
                          <FormattedMessage
                            id="none"
                            defaultMessage="Principal Contact"
                          />
                        }
                        InputProps={{
                          ...params.InputProps,
                          endAdornment: (
                            <Fragment>
                              {loading && <CircularProgress />}
                              {params.InputProps.endAdornment}
                            </Fragment>
                          ),
                        }}
                      />
                    )}
                  />
                )}
                defaultValue={defaultValues?.principalContact}
                rules={{ required: true }}
              />
              <Typography className="font-ebs text-ebs-green-900 font-bold">
                <FormattedMessage id="none" defaultMessage="Addresses" />
                <Tooltip
                  title={
                    <FormattedMessage
                      id="none"
                      defaultMessage="Add another Address"
                    />
                  }
                >
                  <IconButton
                    className="fill-current text-blue-400"
                    onClick={handleAddAddress}
                  >
                    <Add />
                  </IconButton>
                </Tooltip>
              </Typography>
              <div className="col-span-2">
                <div className="flex flex-nowrap">
                  <div className="flex-grow">
                    <div className="grid grid-cols-1 sm:grid-cols-2 gap-2 md:gap-6">
                      <Controller
                        name="country-default"
                        control={control}
                        render={({ field }) => (
                          <Autocomplete
                            {...field}
                            id="country-default"
                            data-testid={"countryDefault"}
                            options={countryList}
                            onChange={(e, options) =>
                              setValue("country-default", options)
                            }
                            defaultValue={
                              defaultValues?.addressDefault?.country
                            }
                            getOptionLabel={(option) => option.name}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                error={errors["country-default"] ? true : false}
                                helperText={
                                  errors["country-default"] && (
                                    <Typography className="text-red-600 text-ebs">
                                      <FormattedMessage
                                        id="none"
                                        defaultMessage="Please select"
                                      />
                                    </Typography>
                                  )
                                }
                                label={
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Country"
                                  />
                                }
                              />
                            )}
                          />
                        )}
                        defaultValue={defaultValues?.addressDefault?.country}
                        rules={{ required: true }}
                      />
                      <Controller
                        name="locality-default"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            data-testid={"localityDefault"}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Locality"
                              />
                            }
                          />
                        )}
                        defaultValue={defaultValues?.addressDefault?.location}
                      />
                      <Controller
                        name="region-default"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            data-testid={"regionDefault"}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Region"
                              />
                            }
                          />
                        )}
                        defaultValue={defaultValues?.addressDefault?.region}
                      />
                      <Controller
                        name="postalCode-default"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            data-testid={"postalCodeDefault"}
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Postal Code"
                              />
                            }
                          />
                        )}
                        defaultValue={defaultValues?.addressDefault?.zipCode}
                      />
                      <Controller
                        name="street-default"
                        control={control}
                        render={({ field }) => (
                          <TextField
                            {...field}
                            required
                            data-testid={"streetDefault"}
                            className="col-span-2"
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Street"
                              />
                            }
                          />
                        )}
                        defaultValue={
                          defaultValues?.addressDefault?.streetAddress
                        }
                      />
                    </div>
                  </div>
                  <div className="grid grid-cols-2 content-center">
                    <div></div>
                    <Controller
                      name="default-default"
                      control={control}
                      render={({ field }) => (
                        <FormControlLabel
                          control={
                            <Checkbox
                              {...field}
                              data-testid={"streetDefault"}
                            />
                          }
                          label={
                            <Typography className="font-ebs">
                              <FormattedMessage
                                id="none"
                                defaultMessage="Default"
                              />
                            </Typography>
                          }
                        />
                      )}
                      defaultValue={defaultValues?.addressDefault?.default}
                    />
                  </div>
                </div>
              </div>
              <div className="col-span-2">
                {address?.map((address, index) => (
                  <>
                    <div className="flex flex-nowrap">
                      <div className="flex-grow">
                        <div className="grid grid-cols-1 sm:grid-cols-2 gap-2 md:gap-6">
                          <Controller
                            name={`country-${index}`}
                            control={control}
                            render={({ field }) => (
                              <Autocomplete
                                {...field}
                                id={`country-${index}`}
                                options={countryList}
                                onChange={(e, options) =>
                                  setValue(`country-${index}`, options)
                                }
                                defaultValue={address?.country}
                                getOptionLabel={(option) => option.name}
                                renderInput={(params) => (
                                  <TextField
                                    {...params}
                                    error={
                                      errors[`country-${index}`] ? true : false
                                    }
                                    helperText={
                                      errors[`country-${index}`] && (
                                        <Typography className="text-red-600 text-ebs">
                                          <FormattedMessage
                                            id="none"
                                            defaultMessage="Please select"
                                          />
                                        </Typography>
                                      )
                                    }
                                    label={
                                      <FormattedMessage
                                        id="none"
                                        defaultMessage="Country"
                                      />
                                    }
                                  />
                                )}
                              />
                            )}
                            defaultValue={address?.country}
                            rules={{ required: true }}
                          />
                          <Controller
                            name={`locality-${index}`}
                            control={control}
                            render={({ field }) => (
                              <TextField
                                {...field}
                                label={
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Locality"
                                  />
                                }
                              />
                            )}
                            defaultValue={address?.location}
                          />
                          <Controller
                            name={`region-${index}`}
                            control={control}
                            render={({ field }) => (
                              <TextField
                                {...field}
                                label={
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Region"
                                  />
                                }
                              />
                            )}
                            defaultValue={address?.region}
                          />
                          <Controller
                            name={`postalCode-${index}`}
                            control={control}
                            render={({ field }) => (
                              <TextField
                                {...field}
                                label={
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Postal Code"
                                  />
                                }
                              />
                            )}
                            defaultValue={address?.zipCode}
                          />
                          <Controller
                            name={`street-${index}`}
                            control={control}
                            render={({ field }) => (
                              <TextField
                                {...field}
                                required
                                className="col-span-2"
                                label={
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Street"
                                  />
                                }
                              />
                            )}
                            defaultValue={address?.streetAddress}
                          />
                        </div>
                      </div>
                      <div className="grid grid-cols-2 content-center">
                        <div>
                          <Tooltip
                            title={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Remove Address"
                              />
                            }
                          >
                            <IconButton
                              className="fill-current text-red-400 flex-grow-0 font-bold"
                              onClick={() =>
                                handleRemoveAddress(address, index)
                              }
                            >
                              <Close />
                            </IconButton>
                          </Tooltip>
                        </div>
                        <Controller
                          name={`default-${index}`}
                          control={control}
                          render={({ field }) => (
                            <FormControlLabel
                              control={<Checkbox {...field} />}
                              label={
                                <Typography className="font-ebs">
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Default"
                                  />
                                </Typography>
                              }
                            />
                          )}
                          defaultValue={address?.default}
                        />
                      </div>
                    </div>
                  </>
                ))}
              </div>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose}>
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Button>
              <Button type="submit">
                <FormattedMessage id="none" defaultMessage="Save" />
              </Button>
            </DialogActions>
          </form>
        </EbsDialog>
      </Box>
    );
  }
);
// Type and required properties
InstitutionMolecule.propTypes = {
  refresh: PropTypes.func.isRequired,
  type: PropTypes.oneOf(["post", "put"]).isRequired,
};
// Default properties
InstitutionMolecule.defaultProps = {
  type: "post",
};

export default memo(InstitutionMolecule);
