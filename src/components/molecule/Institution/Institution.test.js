  import Institution from './Institution';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Institution is in the DOM', () => {
  render(<Institution></Institution>)
  expect(screen.getByTestId('InstitutionTestId')).toBeInTheDocument();
})
