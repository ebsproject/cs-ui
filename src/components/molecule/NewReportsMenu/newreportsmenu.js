import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { Core, Styles, Icons } from "@ebs/styleguide";
// CORE COMPONENTS AND ATOMS TO USE
import AddReportButton from "components/molecule/AddReportButton";
import NewReportBasedOnButton from "components/molecule/NewReportBasedOnButton";
const { IconButton, Menu, Tooltip } = Core;
const { PostAdd } = Icons;
const { makeStyles } = Styles;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  paper: {
    marginRight: theme.spacing(2),
  },
  button: {
    "&:hover": {
      backgroundColor: theme.palette.secondary.main,
    },
    color: theme.palette.common.white,
    backgroundColor: theme.palette.primary.main,
    marginLeft: theme.spacing(1),
    borderRadius: 4,
  },
}));
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const NewReportsMenuMolecule = React.forwardRef(({ refresh }, ref) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const classes = useStyles();

  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    /* 
     @prop data-testid: Id to use inside newreportsmenu.test.js file.
     */
    <div
      ref={ref}
      data-testid={"NewReportsMenuTestId"}
      className={classes.root}
    >
      <Tooltip
        title={<FormattedMessage id="none" defaultMessage="New report" />}
      >
        <IconButton
          className={classes.button}
          aria-controls="new-report-menu"
          aria-haspopup="true"
          color="secondary"
          onClick={handleOpen}
        >
          <PostAdd />
        </IconButton>
      </Tooltip>
      <Menu
        id="new-report-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
      >
        <AddReportButton refresh={refresh} handleMenuClose={handleMenuClose} />
        <NewReportBasedOnButton
          refresh={refresh}
          handleMenuClose={handleMenuClose}
        />
      </Menu>
    </div>
  );
});
// Type and required properties
NewReportsMenuMolecule.propTypes = {
  refresh: PropTypes.func.isRequired,
};
// Default properties
NewReportsMenuMolecule.defaultProps = {};

export default NewReportsMenuMolecule;
