import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { Core, Icons } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import ApolloClient from "utils/apollo";
import { useDispatch } from "react-redux";
import { showMessage } from "store/ducks/message";
import { DELETE_PRODUCT } from "utils/apollo/gql/tenantManagement";
const {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Tooltip,
  Typography,
} = Core;
const { DeleteForever } = Icons;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DeleteProductButtonMolecule = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();

    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const handleDelete = () => {
      ApolloClient.mutate({
        mutation: DELETE_PRODUCT,
        variables: {
          productId: rowData.id,
        },
      })
        .then((response) => {
          // ? No errors, refresh grid and close dialog
          dispatch(
            showMessage({
              message: `Product successfully deleted`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          setOpen(false);
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          setOpen(false);
        });
    };

    return (
      /* 
     @prop data-testid: Id to use inside deleteproductbutton.test.js file.
     */
      <div ref={ref} data-testid={"DeleteProductButtonTestId"}>
        <Tooltip
          title={<FormattedMessage id="none" defaultMessage="Delete product" />}
        >
          <IconButton
            aria-label="DeleteProductButton"
            onClick={handleClickOpen}
            color="secondary"
          >
            <DeleteForever />
          </IconButton>
        </Tooltip>
        <Dialog
          fullWidth
          keepMounted
          maxWidth="sm"
          open={open}
          onClose={handleClose}
        >
          <DialogTitle>
            <FormattedMessage id="none" defaultMessage="Delete Product" />
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <FormattedMessage
                id="none"
                defaultMessage="Are you sure to delete follow product?"
              />
              {rowData.name}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <Typography variant="button" color="secondary">
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Typography>
            </Button>
            <Button onClick={handleDelete}>
              <Typography variant="button">
                <FormattedMessage id="none" defaultMessage="Confirm" />
              </Typography>
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
DeleteProductButtonMolecule.propTypes = {
  rowData: PropTypes.object.isRequired,
  refresh: PropTypes.func.isRequired,
};
// Default properties
DeleteProductButtonMolecule.defaultProps = {};

export default DeleteProductButtonMolecule;
