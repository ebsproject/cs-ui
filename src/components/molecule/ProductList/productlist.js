import React from "react";
import PropTypes from "prop-types";
import { EbsGrid } from "@ebs/components";
import { Core } from "@ebs/styleguide";
import { FIND_DOMAIN } from "utils/apollo/gql/tenantManagement";
import ApolloClient from "utils/apollo";
import ModifyProductButton from "components/molecule/ModifyProductButton";
import { FormattedMessage } from "react-intl";
const { Box, Typography } = Core;

const ProductListMolecule = React.forwardRef(({ rowData }, ref) => {
  const [products, setProducts] = React.useState(null);

  async function fetch({ page, sort, filters }) {
    return new Promise((resolve, reject) => {
      ApolloClient.query({
        query: FIND_DOMAIN,
        variables: { id: rowData.id },
        fetchPolicy: "no-cache",
      })
        .then(({ data }) => {
          const newData = data.findDomain.products
            .slice()
            .sort((a, b) => a.menuOrder - b.menuOrder);
          setProducts(newData);
          resolve({ data: newData, pages: 1, elements: newData.length });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  const columns = [
    { Header: "id", accessor: "id", hidden: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Product" />
        </Typography>
      ),
      csvHeader: "Product",
      accessor: "name",
      filter: true,
      width: 750,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Menu Order" />
        </Typography>
      ),
      csvHeader: "Menu Order",
      accessor: "menuOrder",
      filter: true,
      width: 750,
    },
    { Header: "Icon", accessor: "icon", hidden: true },
    { Header: "Path", accessor: "path", hidden: true },
    {
      Header: "Description",
      accessor: "description",
      hidden: true,
    },
    { Header: "Help", accessor: "help", hidden: true },
  ];

  const rowActions = (data, refresh) => {
    return (
      <Box display="flex">
        <Box>
          <ModifyProductButton
            rowData={data}
            refresh={refresh}
            domainData={rowData}
            productList={products}
          />
        </Box>
      </Box>
    );
  };

  return (
    <div data-testid="ProductListTestId">
      <EbsGrid
        columns={columns}
        fetch={fetch}
        toolbar={false}
        rowactions={rowActions}
        height="20vh"
      />
    </div>
  );
});
// Type and required properties
ProductListMolecule.propTypes = {
  rowData: PropTypes.object.isRequired,
};
// Default properties
ProductListMolecule.defaultProps = {};

export default ProductListMolecule;
