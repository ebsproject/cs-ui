import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import EbsForm from "ebs-form";
import { useQuery } from "@apollo/client";
import { useDispatch, useSelector } from "react-redux";
import ApolloClient from "utils/apollo";
import {
  MODIFY_PRINTOUT_TEMPLATE,
  REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS,
  REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS,
  ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS,
  ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS,
} from "utils/apollo/gql/printoutManager";
import { FIND_PROGRAM_LIST } from "utils/apollo/gql/tenant";
import { printoutClient } from "utils/axios";
import { showMessage } from "store/ducks/message";
const {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Tooltip,
} = Core;
const { Edit } = Icons;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ModifyReportButtonMolecule = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const [programs, setPrograms] = useState(null);
    const [products, setProducts] = useState(null);
    const [defaultValues, setDefaultValues] = useState({
      products: null,
      programs: null,
      datasourceUris: null,
    });
    const dispatch = useDispatch();
    const { tenantInfo, tenantContext } = useSelector(({ tenant }) => tenant);

    useEffect(() => {
      // * Setting all product options formatted
      let productList = [];
      tenantInfo &&
        tenantInfo.instances.map((instance) => {
          instance.domaininstances.map((domainInstance) => {
            domainInstance.domain.products.map((product) => {
              if (
                productList.filter((item) => item.value === product.id)
                  .length === 0
              ) {
                productList.push({
                  label: `${product.domain.name}: ${product.name}`,
                  value: product.id,
                });
              }
            });
          });
        });
      setProducts(productList);
    }, [tenantInfo]);

    useEffect(() => {
      // * Setting default products and programs
      let defaultProducts = [];
      let defaultPrograms = [];
      rowData.products.map((product) => {
        defaultProducts.push({
          label: `${product.domain.name}: ${product.name}`,
          value: product.id,
        });
      });
      rowData.programs.map((program) =>
        defaultPrograms.push({ label: program.name, value: program.id })
      );
      // * Setting default datasource Uri
      getDataSourceUri();
      setDefaultValues({
        ...defaultValues,
        products: defaultProducts,
        programs: defaultPrograms,
      });
    }, [rowData]);

    const getDataSourceUri = () => {
      // * Getting URI datasource of the Report
      printoutClient
        .get(`/api/Report/Datasource?name=${rowData.name}`)
        .then(({ data }) => {
          // * Save textField Uris deffinition (when the report contains more than one datasource uri)
          let textFiledUris = [];
          // * Draw a textfiel for every datasource URI
          data instanceof Array
            ? data.map((uri) =>
                textFiledUris.push({
                  sizes: [12, 12, 12, 12, 12],
                  component: "TextField",
                  name: Object.keys(data)[0],
                  inputProps: {
                    label: `${Object.keys(data)[0]} URI`,
                  },
                  rules: {
                    required: "Datasource URI is required",
                  },
                  defaultValue: uri[Object.keys(data)[0]],
                })
              )
            : textFiledUris.push({
                sizes: [12, 12, 12, 12, 12],
                component: "TextField",
                name: Object.keys(data)[0],
                inputProps: {
                  label: `${Object.keys(data)[0]} URI`,
                },
                rules: {
                  required: "Datasource URI is required",
                },
                defaultValue: data[Object.keys(data)[0]],
              });
          setDefaultValues({
            ...defaultValues,
            datasourceUris: textFiledUris,
          });
        })
        .catch(({ message }) => {});
    };

    const programList = useQuery(FIND_PROGRAM_LIST, {
      variables: {
        tenantId: tenantContext.tenantId,
      },
    });

    if (programList.data && !programs) {
      let programs = [];
      programList.data.findProgramList.content.map((program) =>
        programs.push({ label: program.name, value: program.id })
      );
      setPrograms(programs);
    }

    /*
  Here the function to reset the form values ​​is saved, 
  the reset function comes from the functionality of a Hook 
  which cannot be exported from the EbsForm separately
  */

    function definition({ getValues, setValue, reset }) {
      let components = [
        {
          sizes: [12, 12, 12, 12, 12],
          component: "Select",
          name: "Programs",
          options: programs,
          inputProps: {
            isMulti: true,
            placeholder: "Programs",
          },
          rules: {
            required: "Please select",
          },
          defaultValue: defaultValues.programs,
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "Select",
          name: "Products",
          options: products,
          inputProps: {
            isMulti: true,
            placeholder: "Products",
          },
          rules: {
            required: "Please select",
          },
          defaultValue: defaultValues.products,
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "name",
          inputProps: {
            label: "Name",
            disabled: true,
          },
          rules: {
            required: "A name is required",
          },
          defaultValue: rowData.name,
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "zpl",
          inputProps: {
            label: "ZPL Code",
            multiline: true,
            rows: 5,
          },
          rules: {
            required: "A ZPL Code is required",
          },
          defaultValue: rowData.zpl,
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "description",
          inputProps: {
            label: "Description",
            multiline: true,
            rows: 3,
          },
          rules: {
            required: "A description is required",
          },
          defaultValue: rowData.description,
        },
      ];
      defaultValues.datasourceUris &&
        components.unshift(...defaultValues.datasourceUris);
      return {
        name: "AddReport",
        components: components,
      };
    }

    const mutation = async (formData) => {
      let oldProductIds = [];
      let oldProgramIds = [];
      rowData.products.map((product) => oldProductIds.push(Number(product.id)));
      rowData.programs.map((program) => oldProgramIds.push(Number(program.id)));
      let productIds = [];
      let programIds = [];
      formData.Products.map((product) =>
        productIds.push(Number(product.value))
      );
      formData.Programs.map((program) =>
        programIds.push(Number(program.value))
      );
      // *Update Temmplate Metadata to database
      const { error: ErrorMetadata, data: DataMetadata } =
        await ApolloClient.mutate({
          mutation: MODIFY_PRINTOUT_TEMPLATE,
          variables: {
            printoutTemplate: {
              id: Number(rowData.id),
              name: rowData.name,
              description: formData.description,
              zpl: formData.zpl,
              tenantId: Number(tenantContext.tenantId),
            },
          },
        });
      // *Removing old products
      const { error: ErrorRemoveProduct, data: DataRemoveProduct } =
        await ApolloClient.mutate({
          mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS,
          variables: {
            printoutTemplateId: rowData.id,
            productIds: oldProductIds,
          },
        });
      // *Removing old programs
      const { error: ErrorRemoveProgram, data: DataRemoveProgram } =
        await ApolloClient.mutate({
          mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS,
          variables: {
            printoutTemplateId: rowData.id,
            programIds: oldProgramIds,
          },
        });
      // *Adding printout template Id to products
      const { error: ErrorProducts, data: DataProducts } =
        await ApolloClient.mutate({
          mutation: ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS,
          variables: {
            printoutTemplateId: Number(rowData.id),
            productIds: productIds,
          },
        });
      // *Adding printout template Id to programs
      const { error: ErrorPrograms, data: DataPrograms } =
        await ApolloClient.mutate({
          mutation: ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS,
          variables: {
            printoutTemplateId: Number(rowData.id),
            programIds: programIds,
          },
        });
      // * Updating Datasource Uri if exist
      defaultValues.datasourceUris &&
        defaultValues.datasourceUris.map((uri) => {
          printoutClient
            .put(
              `/api/Report/Datasource?name=${rowData.name}&uri=${
                formData[uri.name]
              }`
            )
            .then((res) => {
              // ? No errors, refresh grid and close form
              if (
                !ErrorMetadata &&
                !ErrorPrograms &&
                !ErrorProducts &&
                !ErrorRemoveProduct &&
                !ErrorRemoveProgram
              ) {
                dispatch(
                  showMessage({
                    message: `Report successfully updated`,
                    variant: "success",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                  })
                );
                setOpen(false);
                refresh();
              }
            })
            .catch(({ message }) => {
              dispatch(
                showMessage({
                  message: message,
                  variant: "error",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
            });
        });
    };

    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };
    return (
      /* 
     @prop data-testid: Id to use inside modifyreportbutton.test.js file.
     */
      <div ref={ref} data-testid={"ModifyReportButtonTestId"}>
        <Tooltip
          title={<FormattedMessage id="none" defaultMessage="Modify report" />}
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="modify"
            color="primary"
          >
            <Edit />
          </IconButton>
        </Tooltip>
        <Dialog onClose={handleClose} open={open}>
          <DialogTitle>
            <FormattedMessage id="none" defaultMessage="Modify Report" />
          </DialogTitle>
          <DialogContent>
            <EbsForm onSubmit={mutation} definition={definition}>
              <DialogActions>
                <Button onClick={handleClose} color="secondary">
                  <FormattedMessage id="none" defaultMessage="Close" />
                </Button>
                <Button type="submit">
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </DialogActions>
            </EbsForm>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
ModifyReportButtonMolecule.propTypes = {
  rowData: PropTypes.object.isRequired,
  refresh: PropTypes.func.isRequired,
};
// Default properties
ModifyReportButtonMolecule.defaultProps = {};

export default ModifyReportButtonMolecule;
