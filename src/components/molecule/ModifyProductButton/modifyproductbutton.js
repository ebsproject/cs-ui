import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import Autocomplete from "@material-ui/lab/Autocomplete";
import EbsForm from "ebs-form";
import { useQuery } from "@apollo/client";
import { useDispatch } from "react-redux";
import ApolloClient from "utils/apollo";
import { FIND_DOMAIN, MODIFY_PRODUCT } from "utils/apollo/gql/tenantManagement";
import { showMessage } from "store/ducks/message";
const {
  Icon,
  Dialog,
  IconButton,
  Tooltip,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
  Typography,
  DialogActions,
} = Core;
const { Edit } = Icons;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ModifyProductButtonMolecule = React.forwardRef(
  ({ domainData, rowData, refresh }, ref) => {
    const [open, setOpen] = React.useState(false);
    const [icons, setIcons] = React.useState([]);
    const [icon, setIcon] = React.useState(rowData.icon);
    const [iconInput, setIconInput] = React.useState(rowData.icon);
    const dispatch = useDispatch();

    const { data, error } = useQuery(FIND_DOMAIN, {
      variables: {
        id: domainData.id,
      },
    });

    React.useEffect(() => {
      // * Filtering icons list
      let newIconsList = Object.keys(Icons).filter((icon) => {
        if (
          !_.endsWith(icon, "Outlined") &&
          !_.endsWith(icon, "Rounded") &&
          !_.endsWith(icon, "TwoTone") &&
          !_.endsWith(icon, "Sharp")
        ) {
          return icon;
        }
      });
      setIcons(newIconsList);
    }, []);

    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const definition = ({ getValues, setValue, reset }) => {
      return {
        name: "AddProduct",
        components: [
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "domain",
            inputProps: {
              "data-testid": "fromDomain",
              label: "From Domain",
              disabled: true,
            },
            rules: {
              required: "A name is required",
            },
            defaultValue: data && data.findDomain && data.findDomain.name,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "name",
            inputProps: {
              "data-testid": "name",
              label: "Product Name",
            },
            rules: {
              required: "A name is required",
              validate: (value) => {
                let isValidName = null;
                data &&
                  data.findDomain &&
                  data.findDomain.products.map((product) => {
                    isValidName =
                      product.name === value
                        ? "This product aready exist"
                        : true;
                  });
                return isValidName;
              },
            },
            defaultValue: rowData.name,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "description",
            inputProps: {
              "data-testid": "description",
              label: "Description",
              multiline: true,
              rows: 3,
            },
            rules: {
              required: "A description is required",
            },
            defaultValue: rowData.description,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "help",
            inputProps: {
              "data-testid": "help",
              label: "Help",
              multiline: true,
              rows: 2,
            },
            rules: {
              required: "A help is required",
            },
            defaultValue: rowData.help,
          },
          {
            sizes: [10, 10, 10, 10, 10],
            component: "TextField",
            name: "path",
            inputProps: {
              "data-testid": "path",
              label: "Path",
            },
            rules: {
              required: "A path is required",
              validate: (value) => {
                if (value.length < 251) {
                  return true;
                } else {
                  return "Path too long";
                }
              },
            },
            defaultValue: rowData.path,
          },
          {
            sizes: [2, 2, 2, 2, 2],
            component: "TextField",
            name: "menuOrder",
            inputProps: {
              "data-testid": "menuOrder",
              label: "Menu Order",
              type: "Number",
            },
            rules: {
              required: "A menu order is required",
            },
            defaultValue: rowData.menuOrder,
          },
        ],
      };
    };

    const mutation = (formData) => {
      ApolloClient.mutate({
        mutation: MODIFY_PRODUCT,
        variables: {
          ProductInput: {
            ...formData,
            mainEntity: "N/A",
            domainId: Number(domainData.id),
            icon: icon,
            htmltagId: 1,
            id: rowData.id,
          },
        },
      })
        .then(({ data }) => {
          handleClose();
          refresh();
          dispatch(
            showMessage({
              message: "Product modified sucessfully",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: `${message}`,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    };

    const handleChange = (event, value, reason) => {
      setIcon(value);
    };

    const handleInputChange = (event, newInputValue) => {
      setIconInput(newInputValue);
    };

    return (
      /* 
     @prop data-testid: Id to use inside modifyproductbutton.test.js file.
     */
      <div ref={ref} data-testid={"ModifyProductButtonTestId"}>
        <Tooltip
          title={<FormattedMessage id="none" defaultMessage="Modify Product" />}
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="modify-product"
            color="primary"
          >
            <Edit />
          </IconButton>
        </Tooltip>
        <Dialog onClose={handleClose} open={open}>
          <DialogTitle>
            <FormattedMessage id="none" defaultMessage="Modify Product" />
          </DialogTitle>
          <DialogContent>
            <EbsForm onSubmit={mutation} definition={definition}>
              <br />
              <Autocomplete
                id="autocomplete-icon-list"
                fullWidth
                value={icon}
                options={icons}
                autoHighlight
                onChange={handleChange}
                getOptionLabel={(option) => option}
                inputValue={iconInput}
                onInputChange={handleInputChange}
                renderOption={(option) => (
                  <React.Fragment>
                    <Icon fontSize="large">
                      {_.lowerCase(option).replace(" ", "_")}
                    </Icon>
                    <Typography variant="subtitle1">{option}</Typography>
                  </React.Fragment>
                )}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Choose an icon"
                    variant="outlined"
                    InputLabelProps={{ shrink: true }}
                    inputProps={{
                      ...params.inputProps,
                      autoComplete: "new-password",
                    }}
                  />
                )}
              />
              <DialogActions>
                <Button onClick={handleClose} color="secondary">
                  <FormattedMessage id="none" defaultMessage="Close" />
                </Button>
                <Button type="submit">
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </DialogActions>
            </EbsForm>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
ModifyProductButtonMolecule.propTypes = {
  domainData: PropTypes.object.isRequired,
  rowData: PropTypes.object.isRequired,
  refresh: PropTypes.func.isRequired,
};
// Default properties
ModifyProductButtonMolecule.defaultProps = {};

export default ModifyProductButtonMolecule;
