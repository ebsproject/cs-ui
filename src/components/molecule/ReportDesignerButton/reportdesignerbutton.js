import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { Core, Styles, Icons } from "@ebs/styleguide";
import ReportDesigner from "components/atoms/ReportDesigner";
const { makeStyles } = Styles;
const { Close, SettingsApplications } = Icons;
const {
  AppBar,
  Dialog,
  DialogContent,
  IconButton,
  Slide,
  Toolbar,
  Typography,
  Tooltip,
} = Core;
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  dialogContent: {
    padding: "50px",
  },
}));

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ReportDesignerButtonMolecule = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const [open, setOpen] = React.useState(false);
    const classes = useStyles();
    const handleOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
      refresh();
    };
    return (
      /* 
     @prop data-testid: Id to use inside reportdesignerbutton.test.js file.
     */
      <div ref={ref} data-testid={"ReportDesignerButtonTestId"}>
        <Tooltip title={<FormattedMessage id="none" defaultMessage="Design" />}>
          <IconButton
            aria-label="permission"
            color="primary"
            onClick={handleOpen}
          >
            <SettingsApplications />
          </IconButton>
        </Tooltip>
        <Dialog
          fullScreen
          open={open}
          onClose={handleClose}
          TransitionComponent={Transition}
        >
          <AppBar>
            <Toolbar>
              <IconButton
                edge="start"
                color="inherit"
                onClick={handleClose}
                aria-label="close"
              >
                <Close />
              </IconButton>
              <Typography variant="h6">
                <FormattedMessage id="none" defaultMessage="Designer" />
              </Typography>
            </Toolbar>
          </AppBar>
          <DialogContent className={classes.dialogContent}>
            <ReportDesigner reportName={rowData.name} />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
ReportDesignerButtonMolecule.propTypes = {
  rowData: PropTypes.object.isRequired,
  refresh: PropTypes.func.isRequired,
};
// Default properties
ReportDesignerButtonMolecule.defaultProps = {};

export default ReportDesignerButtonMolecule;
