import React, {useState, useEffect} from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
import EbsForm from "ebs-form";
import { useQuery } from "@apollo/client";
import { useDispatch, useSelector } from "react-redux";
import ApolloClient from "utils/apollo";
import {
  CREATE_PRINTOUT_TEMPLATE,
  ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS,
  ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS,
  FIND_PRINTOUT_TEMPLATE_LIST,
} from "utils/apollo/gql/printoutManager";
import { FIND_TENANT } from "utils/apollo/gql/tenant";
import { FIND_PROGRAM_LIST } from "utils/apollo/gql/tenant";
import { showMessage } from "store/ducks/message";
import { printoutClient } from "utils/axios";
const { Button, Dialog, DialogActions, DialogContent, DialogTitle, MenuItem } =
  Core;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const NewReportBasedOnButtonMolecule = React.forwardRef(
  ({ handleMenuClose, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const [programs, setPrograms] = useState(null);
    const [products, setProducts] = useState(null);
    const [templates, setTemplates] = useState(null);
    const dispatch = useDispatch();
    const { tenantContext } = useSelector(({ tenant }) => tenant);

    const { loading, error, data } = useQuery(FIND_TENANT, {
      variables: { id: Number(tenantContext.tenantId) },
    });

    useEffect(() => {
      let templates = [];
      ApolloClient.query({
        query: FIND_PRINTOUT_TEMPLATE_LIST,
        variables: {
          page: { number: 1, size: 1000 },
          sort: { col: "id", mod: "DES" },
        },
        fetchPolicy: "no-cache",
      })
        .then(({ data }) => {
          data.findPrintoutTemplateList.content.map((template) =>
            templates.push({ value: template.id, label: template.name })
          );
          setTemplates(templates);
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: `${message}`,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }, [open]);

    useEffect(() => {
      // * Setting all product options formatted
      let productList = [];
      data &&
        data.findTenant.instances.map((instance) => {
          instance.domaininstances.map((domainInstance) => {
            domainInstance.domain.products.map((product) => {
              if (
                productList.filter((item) => item.value === product.id)
                  .length === 0
              ) {
                productList.push({
                  label: `${product.domain.name}: ${product.name}`,
                  value: product.id,
                });
              }
            });
          });
        });
      setProducts(productList);
    }, [data]);

    const programList = useQuery(FIND_PROGRAM_LIST, {
      variables: {
        tenantId: tenantContext.tenantId,
      },
    });

    if (programList.data && !programs) {
      let programs = [];
      programList.data.findProgramList.content.map((program) =>
        programs.push({ label: program.name, value: program.id })
      );
      setPrograms(programs);
    }

    function definition({ getValues, setValue, reset }) {
      return {
        name: "AddReport",
        components: [
          {
            sizes: [12, 12, 12, 12, 12],
            component: "Select",
            name: "template",
            options: templates,
            inputProps: {
              "aria-label": "template",
              placeholder: "Template based on",
            },
            rules: {
              required: "Please select",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "Select",
            name: "Programs",
            options: programs,
            inputProps: {
              "aria-label": "Programs",
              isMulti: true,
              placeholder: "Programs",
            },
            rules: {
              required: "Please select",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "Select",
            name: "Products",
            options: products,
            inputProps: {
              "aria-label": "Products",
              isMulti: true,
              placeholder: "Products",
            },
            rules: {
              required: "Please select",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "name",
            inputProps: {
              "data-testid": "name",
              label: "Name",
            },
            rules: {
              required: "A name is required",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "zpl",
            inputProps: {
              "data-testid": "zpl",
              label: "ZPL Code",
              multiline: true,
              rows: 5,
            },
            rules: {
              required: "A ZPL Code is required",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "description",
            inputProps: {
              "data-testid": "description",
              label: "Description",
              multiline: true,
              rows: 3,
            },
            rules: {
              required: "A description is required",
            },
          },
        ],
      };
    }

    const mutation = (formData) => {
      let productIds = [];
      let programIds = [];
      formData.Products.map((product) =>
        productIds.push(Number(product.value))
      );
      formData.Programs.map((program) =>
        programIds.push(Number(program.value))
      );
      // Create template to avoid duplicated names
      printoutClient
        .post(
          `/api/Report/Copy?source=${formData.template.label}&destination=${formData.name}`
        )
        .then(({ status }) => {
          if (status === 200) {
            // Save Temmplate Metadata to database
            ApolloClient.mutate({
              mutation: CREATE_PRINTOUT_TEMPLATE,
              variables: { ...formData, tenantId: Number(tenantContext.tenantId) },
            })
              .then(({ data }) => {
                // Adding printout template Id to products
                ApolloClient.mutate({
                  mutation: ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS,
                  variables: {
                    printoutTemplateId: Number(data.createPrintoutTemplate.id),
                    productIds: productIds,
                  },
                }).catch(({ message }) => {
                  dispatch(
                    showMessage({
                      message: `${message}`,
                      variant: "error",
                      anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                      },
                    })
                  );
                });
                // Adding printout template Id to programs
                ApolloClient.mutate({
                  mutation: ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS,
                  variables: {
                    printoutTemplateId: Number(data.createPrintoutTemplate.id),
                    programIds: programIds,
                  },
                }).catch(({ message }) => {
                  dispatch(
                    showMessage({
                      message: `${message}`,
                      variant: "error",
                      anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                      },
                    })
                  );
                });
              })
              .catch(({ message }) => {
                dispatch(
                  showMessage({
                    message: `${message}`,
                    variant: "error",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                  })
                );
              })
              .then(() => {
                // All saved success
                dispatch(
                  showMessage({
                    message: "Template successfully saved",
                    variant: "success",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                  })
                );
                setOpen(false);
                refresh();
              });
          }
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: `${message}`,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    };

    const handleClickOpen = () => {
      handleMenuClose();
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    return (
      /* 
     @prop data-testid: Id to use inside newreportbasedonbutton.test.js file.
     */
      <React.Fragment>
        <MenuItem
          onClick={handleClickOpen}
          ref={ref}
          data-testid={"NewReportBasedOnButtonTestId"}
        >
          <FormattedMessage id="none" defaultMessage="New report based on..." />
        </MenuItem>
        <Dialog onClose={handleClose} open={open} aria-label="addReportDialog">
          <DialogTitle>
            <FormattedMessage
              id="none"
              defaultMessage="New Report based on..."
            />
          </DialogTitle>
          <DialogContent>
            <EbsForm onSubmit={mutation} definition={definition}>
              <DialogActions>
                <Button onClick={handleClose} color="secondary">
                  <FormattedMessage id="none" defaultMessage="Close" />
                </Button>
                <Button type="submit">
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </DialogActions>
            </EbsForm>
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
);
// Type and required properties
NewReportBasedOnButtonMolecule.propTypes = {
  handleMenuClose: PropTypes.func.isRequired,
  refresh: PropTypes.func.isRequired,
};
// Default properties
NewReportBasedOnButtonMolecule.defaultProps = {};

export default NewReportBasedOnButtonMolecule;
