import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
import { deleteContact } from "store/ducks/crm";
import { useDispatch, useSelector, useStore } from "react-redux";
// CORE COMPONENTS
const {
  DialogContent,
  Typography,
  DialogActions,
  Button,
  IconButton,
  Tooltip,
} = Core;
const { Delete } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const DeleteContactMolecule = React.forwardRef(
  ({ refresh, contactId }, ref) => {
    const [open, setOpen] = useState(false);
    const { success } = useSelector(({ crm }) => crm);
    const { getState } = useStore();
    const dispatch = useDispatch();
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [success]);
    const submit = () => {
      deleteContact(contactId)(dispatch, getState);
    };
    return (
      /* 
     @prop data-testid: Id to use inside DeleteContact.test.js file.
     */
      <div ref={ref} data-testid={"DeleteContactTestId"}>
        <Tooltip
          title={<FormattedMessage id="none" defaultMessage="Delete Person" />}
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="delete-person"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth='sm'
          title={<FormattedMessage id="none" defaultMessage="Delete Person" />}
        >
          <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage="Do you want to delete this person permanently?"
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submit}>
              <FormattedMessage id="none" defaultMessage="Submit" />
            </Button>
          </DialogActions>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
DeleteContactMolecule.propTypes = {
  refresh: PropTypes.func.isRequired,
  contactId: PropTypes.number.isRequired,
};
// Default properties
DeleteContactMolecule.defaultProps = {};

export default DeleteContactMolecule;
