import React, { useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
import CardTenant from "components/atoms/CardTenant";
import { FormattedMessage } from "react-intl";
import { useSelector, useDispatch, useStore } from "react-redux";
import { setTenantContext } from "store/ducks/tenant";
import Progress from "components/atoms/Progress";
const { Grid, Typography, Button } = Core;

const TenantListOrganism = React.forwardRef(({}, ref) => {
  const dispatch = useDispatch();
  const { getState } = useStore();
  const { userInfo } = useSelector(({ user }) => user);

  const handleCardClick = (e) => {
    e.preventDefault();
    setTenantContext({ tenantId: Number(e.currentTarget.id) })(
      dispatch,
      getState
    );
  };

  // ? There is only one tenant
  useEffect(() => {
    userInfo.tenants.length === 1 &&
      setTenantContext({ tenantId: Number(userInfo.tenants[0].id) })(
        dispatch,
        getState
      );
  }, [userInfo]);

  //* There isn't userInfo
  if (!userInfo) {
    return <Progress />;
  } else if (userInfo.tenants.length === 1) {
    return <Fragment />;
  } else {
    return (
      <Grid
        className={
          "grid grid-cols-1 sm:grid-cols-3 w-full flex-wrap flex-col justify-center justify-items-center"
        }
      >
        <Grid>
          <Typography variant="h4">
            <FormattedMessage
              id="tnt.comp.comsoon.welcome"
              defaultMessage="Select an organization"
            />
          </Typography>
        </Grid>
        {userInfo.tenants
          .slice()
          .sort(function (a, b) {
            return a.id - b.id;
          })
          .map((tenant, index) => (
            <Grid key={tenant.name}>
              <CardTenant key={index} ref={ref} tenantId={Number(tenant.id)}>
                <Button
                  size="small"
                  color="primary"
                  id={tenant.id}
                  onClick={handleCardClick}
                >
                  <FormattedMessage
                    id="tnt.tenant.select"
                    defaultMessage="Select"
                  />
                </Button>
              </CardTenant>
            </Grid>
          ))}
      </Grid>
    );
  }
});
// Type and required properties
TenantListOrganism.propTypes = {
  tenants: PropTypes.array,
  children: PropTypes.node,
};
// Default properties
TenantListOrganism.defaultProps = {
  tenants: [],
  children: null,
};

export default TenantListOrganism;
