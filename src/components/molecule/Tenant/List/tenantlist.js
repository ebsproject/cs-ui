import React from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
import ModifyTenant from "components/molecule/ModifyTenantButton";
const { Box, Typography } = Core;

const TenantListMolecule = React.forwardRef((props, ref) => {
  const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Registration ID" />
        </Typography>
      ),
      csvHeader: "Registration ID",
      accessor: "name",
      width: 250,
    },
    {
      Header: "OrganizationId",
      accessor: "organization.id",
      disableGlobalFilter: true,
      hidden: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organization" />
        </Typography>
      ),
      csvHeader: "Organization",
      accessor: "organization.name",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Legal Name" />
        </Typography>
      ),
      csvHeader: "Legal Name",
      accessor: "organization.legalName",
      disableGlobalFilter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Logo" />
        </Typography>
      ),
      csvHeader: "Logo",
      accessor: "organization.logo",
      disableGlobalFilter: true,
      width: 200,
    },
    {
      Header: "CustomerId",
      accessor: "customer.id",
      disableGlobalFilter: true,
      hidden: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Customer" />
        </Typography>
      ),
      csvHeader: "Customer",
      accessor: "customer.name",
      disableGlobalFilter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Customer Logo" />
        </Typography>
      ),
      csvHeader: "Customer Logo",
      accessor: "customer.logo",
      disableGlobalFilter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Expiration" />
        </Typography>
      ),
      csvHeader: "Expiration",
      accessor: "expiration",
      disableGlobalFilter: true,
      width: 200,
    },
  ];

  const rowActions = (rowData, refresh) => {
    return (
      <Box>
        <Box>
          <ModifyTenant rowData={rowData} refresh={refresh} />
        </Box>
      </Box>
    );
  };

  return (
    <EbsGrid
      toolbar={true}
      columns={columns}
      entity="Tenant"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Registers" />
        </Typography>
      }
      uri={process.env.REACT_APP_CSAPI_URI_GRAPHQL}
      // toolbaractions={toolbarActions}
      select="multi"
      rowactions={rowActions}
      callstandard="graphql"
      height="65vh"
    />
  );
});
// Type and required properties
TenantListMolecule.propTypes = {};
// Default properties
TenantListMolecule.defaultProps = {};

export default TenantListMolecule;
