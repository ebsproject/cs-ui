import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS AND ATOMS TO USE
import { useSelector, useDispatch, useStore } from "react-redux";
import { findTenant } from "store/ducks/tenant";
import { FormattedMessage } from "react-intl";
const { Grid, Typography } = Core;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const TenantInfoMolecule = React.forwardRef(({}, ref) => {
  const dispatch = useDispatch();
  const { getState } = useStore();
  const { tenantInfo, tenantContext } = useSelector(({ tenant }) => tenant);
  const { userInfo } = useSelector(({ user }) => user);
  // * Set tenantInfo
  useEffect(() => {
    tenantContext.tenantId &&
      findTenant(tenantContext.tenantId)(dispatch, getState);
  }, [tenantInfo]);

  return (
    <Grid className="flex flex-col justify-center justify-items-center">
      <Grid className="flex flex-row flex-wrap justify-center justify-items-center">
        <Typography variant="h5" className="ebs">
          {userInfo?.userName}
        </Typography>
        <Typography variant="h5" className="ebs">
          <FormattedMessage
            id="tnt.comp.comsoon.welcome"
            defaultMessage=", is part of the of:"
          />
        </Typography>
      </Grid>
      <Grid className="flex flex-row flex-wrap justify-center justify-items-center">
        <Typography variant="h6" className="ebs">
          <FormattedMessage id="none" defaultMessage="Customer Name" />
        </Typography>
        <Typography
          variant="h6"
          className="ebs"
        >{`: ${tenantInfo?.customer.name}`}</Typography>
      </Grid>
      <Grid className="flex flex-row flex-wrap justify-center justify-items-center">
        <Typography variant="h6" className="ebs">
          <FormattedMessage id="none" defaultMessage="Organization Name" />
        </Typography>
        <Typography variant="h6" className="ebs">
          {`: ${tenantInfo?.organization.name}`}
        </Typography>
      </Grid>
      <Grid className="flex flex-row flex-wrap justify-center justify-items-center">
        <Typography variant="h6" className="ebs">
          <FormattedMessage id="none" defaultMessage="Admin Contact" />
        </Typography>
        <Typography variant="h6" className="ebs">
          {`: ${tenantInfo?.customer.officialEmail}`}
        </Typography>
      </Grid>
    </Grid>
  );
});
// Type and required properties
TenantInfoMolecule.propTypes = {};
// Default properties
TenantInfoMolecule.defaultProps = {};

export default TenantInfoMolecule;
