import React, { Fragment, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Core } from "@ebs/styleguide";
import { findInstance } from "store/ducks/tenant";
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector, useStore } from "react-redux";
import { setTenantContext } from "store/ducks/tenant";
import Progress from "components/atoms/Progress";
import { showMessage } from "store/ducks/message";
const { Grid, Typography, Card, CardContent, CardMedia, CardActionArea, Box } =
  Core;
// Import all Icons
const iconList = require.context("assets/images/domains", true, /\.svg$/);
const icons = iconList.keys().reduce((images, path) => {
  images[path] = iconList(path);
  return images;
}, {});

const ProductListMolecule = React.forwardRef(({}, ref) => {
  const dispatch = useDispatch();
  const { getState } = useStore();
  const { loading, error, instanceInfo, tenantContext } = useSelector(
    ({ tenant }) => tenant
  );
  const history = useHistory();

  useEffect(() => {
    tenantContext.instanceId &&
      findInstance(tenantContext.instanceId)(dispatch, getState);
  }, [tenantContext.instanceId]);

  if (loading) {
    return <Progress />;
  } else if (!instanceInfo || error) {
    return <Fragment />;
  }

  //events
  const handleClickDomain = (item) => {
    if (item.mfe) {
      setTenantContext({ domainId: Number(item.domain.id) })(
        dispatch,
        getState
      );
      history.push(`/${item.domain.prefix}`);
    } else {
      item.context
        ? window.open(item.context, "__blank")
        : dispatch(
            showMessage({
              message: "The component does not have URL",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
    }
  };

  return (
    <Grid className="grid-cols-1 gap-14 sm:gap-16">
      <Grid className="flex-grow justify-center content-center rounded-sm bg-ebs-green-default">
        <Typography
          variant="h6"
          className="ebs text-white text-center place-self-center py-8"
        >
          <FormattedMessage
            id="tnt.comp.comsoon.welcome"
            defaultMessage={`EBS Services for ${instanceInfo.name} Instance`}
          />
        </Typography>
      </Grid>
      <Grid>
        <Box className="flex flex-nowrap sm:justify-center overflow-x-scroll">
          {instanceInfo.domaininstances
            .slice()
            .sort(function (a, b) {
              return a.id - b.id;
            })
            .map((domain) => (
              <CardActionArea
                onClick={() => handleClickDomain(domain)}
                key={domain.id}
                className="sm:w-1/5 sm:h-1/6 transition duration-200 ease-in-out transform hover:-translate-y-1 hover:scale-110 rounded sm:rounded-fully px-6 md:rounded-lg"
              >
                <Card className="rounded-sm hover:shadow-sm ">
                  <CardMedia className="w-100 h-100" title={domain.domain.info}>
                    <img src={icons[`./${domain.domain.icon}`]} />
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" className="ebs">
                      {domain.domain.info}
                    </Typography>
                    <Typography variant="body2" className="ebs">
                      {domain.domain.info}
                    </Typography>
                  </CardContent>
                </Card>
              </CardActionArea>
            ))}
        </Box>
      </Grid>
    </Grid>
  );
});

export default ProductListMolecule;
