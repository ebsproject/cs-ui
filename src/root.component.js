import React, { useState } from "react";
import { Provider } from "react-redux";
import { ErrorBoundary } from "react-error-boundary";
import { IntlProvider } from "react-intl";
import Routes from "routes/Routes";
import ErrorFallback from "error/ErrorFallback";
import { EBSMaterialUIProvider } from "@ebs/styleguide";
//duck store
import store from "./store";

export default function Root(props) {
  const [state, setState] = useState({
    locale: localStorage.getItem("locale") || "en",
    message: localStorage.getItem("language"),
  });
  // Hell world! from Michoacan xD
  return (
    <EBSMaterialUIProvider>
      <Provider store={store}>
        <IntlProvider
          locale={state.locale}
          messages={state.messages}
          defaultLocale={"en"}
        >
          <ErrorBoundary FallbackComponent={ErrorFallback}>
            <Routes />
          </ErrorBoundary>
        </IntlProvider>
      </Provider>
    </EBSMaterialUIProvider>
  );
}
