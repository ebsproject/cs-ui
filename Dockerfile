FROM nginx:alpine AS builder

WORKDIR /usr/app

RUN apk update && apk add bash  && apk add yarn && apk add npm

COPY . .

RUN npm install && npm run build

FROM nginx:alpine

WORKDIR /usr/share/nginx/html
 
RUN rm -rf *

COPY --from=builder  /usr/app/dist .

COPY --from=builder /usr/app/default.conf /etc/nginx/conf.d/default.conf

RUN chown nginx:nginx -R /usr/share/nginx/html

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]
