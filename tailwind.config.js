module.exports = {
  purge: ["./src/**/*.html", "./src/**/*.js"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        transparent: "transparent",
        current: "currentColor",
        "ebs-splash": "#121212",
        "ebs-green": {
          default: "#009688",
          900: "#00695C",
        },
        "ebs-brand": {
          default: "#6FBF3D",
          900: "#3dbf4c",
        },
        "ebs-gray": {
          default: "#535461",
        },
      },
      fontFamily: {
        ebs: ["Raleway"],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
