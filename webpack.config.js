const path = require("path");
const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react");
const DotenvWebpackPlugin = require("dotenv-webpack");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "ebs",
    projectName: "cs",
    webpackConfigEnv,
    argv,
  });

  // add to defaultConfig externals 'cs-ui'
  defaultConfig.externals.push(
    "@ebs/styleguide",
    "@ebs/components"
  );
  const cssRule = defaultConfig.module.rules.find(
    (r) =>
      Array.isArray(r.use) && r.use.find((u) => u.loader.includes("css-loader"))
  );
  cssRule.use.push({
    loader: "postcss-loader",
  });

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif|svg|ttf|eot|woff|woff2)$/i,
          type: "asset/resource",
        },
      ],
    },
    resolve: {
      alias: {
        "@history": path.resolve(__dirname, "./src/@history/"),
        "@lodash": path.resolve(__dirname, "./src/@lodash/"),
        assets: path.resolve(__dirname, "./src/assets/"),
        auth: path.resolve(__dirname, "./src/auth/"),
        components: path.resolve(__dirname, "./src/components/"),
        context: path.resolve(__dirname, "./src/context/"),
        error: path.resolve(__dirname, "./src/error/"),
        layout: path.resolve(__dirname, "./src/layout/"),
        page: path.resolve(__dirname, "./src/page/"),
        routes: path.resolve(__dirname, "./src/routes/"),
        services: path.resolve(__dirname, "./src/services/"),
        store: path.resolve(__dirname, "./src/store/"),
        utils: path.resolve(__dirname, "./src/utils/"),
      },
    },
    plugins: [new DotenvWebpackPlugin()],
  });
};
