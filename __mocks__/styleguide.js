import { forwardRef, createElement } from "react";

export const Core = {
  AppBar: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  List: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Card: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Grid: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  CardHeader: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  CardContent: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  CardMedia: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Toolbar: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Button: forwardRef(({ children, ...rest }, ref) => (
    <button {...rest}>{children}</button>
  )),
  ListItem: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Snackbar: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  LinearProgress: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  SnackbarContent: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  DialogActions: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  DialogContent: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Icon: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  IconButton: forwardRef(({ children, ...rest }, ref) => (
    <button {...rest}>{children}</button>
  )),
  TextField: forwardRef(
    (
      {
        helperText,
        fullWidth,
        InputLabelProps,
        InputProps,
        inputProps,
        error,
        ...rest
      },
      ref
    ) => createElement("input", { ...rest })
  ),
  Tooltip: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Typography: forwardRef(({ children, ...rest }, ref) => (
    <label {...rest}>{children}</label>
  )),
  FormControl: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  InputLabel: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Input: forwardRef(
    ({ inputComponent, InputLabelProps, InputProps, ...rest }, ref) =>
      createElement("input", { ...rest })
  ),
  Checkbox: forwardRef(({ inputProps, ...rest }, ref) =>
    createElement("input", { ...rest })
  ),
  FormControlLabel: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  //   Typography: forwardRef(({ children,...rest }, ref) =>     <>{children}</>),
  //   Typography: forwardRef(({ children,...rest }, ref) =>     <>{children}</>),
  //   Typography: forwardRef(({ children,...rest }, ref) =>     <>{children}</>),
};
export const Styles = {
  makeStyles: jest.fn().mockImplementation((callback) => {
    callback({
      options: { common: { fonts: { sizes: {} } } },
      spacing: jest.fn(),
      palette: { primary: {}, secondary: {},common:{} },
    }); // this will execute the fn passed in which is missing the coverage
    return jest.fn().mockReturnValue({ root: "" }); // here the expected MUI styles });
  }),
};
export const Icons = {
  Add: forwardRef((props, ref) => <div {...props} />),
  Close: forwardRef((props, ref) => <div {...props} />),
  PostAdd: forwardRef((props, ref) => <div {...props} />),
  Edit: forwardRef((props, ref) => <div {...props} />),
};
export const Colors = {};

export const EbsDialog = forwardRef(({ children, title, ...rest }, ref) => (
  <>
    <label>{title}</label>
    {children}
  </>
));

export const EbsTabsLayout = forwardRef(({ tabs, ...rest }, ref) => (
  <div {...rest} ref={ref}>
    {tabs.map((tab, key) => (
      <div key={key}>
        <label>{tab.label}</label>
        {tab.component}
      </div>
    ))}
  </div>
));
